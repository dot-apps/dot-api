@extends('layouts.AdminLayout')

@section('title')
Partner - {{ $partner->name }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/partners">partners</a></li>
    <li class="breadcrumb-item active">Detail partner</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Detail Parner</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group form-group-default required">
                    <label for="full_name">Full Name</label>
                    {{ $partner->name }}
                </div>
                <div class="form-group form-group-default required">
                    <label for="email">Email address</label>
                    {{ $partner->email }}
                </div>
                <div class="form-group form-group-default required">
                    <label for="pass">phone</label>
                    {{ $partner->phone }}
                </div>
                <div class="form-group form-group-default required">
                    <label for="pass">Desc/Note</label>
                    {{ $partner->desc }}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
