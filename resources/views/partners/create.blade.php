@extends('layouts.adminLayout')

@section('title')
New Partner
@endsection


@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/partners">partners</a></li>
    <li class="breadcrumb-item active">Add New</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add new Partner</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="/partners" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group form-group-default required">
                        <label for="full_name">Full Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ old('name') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="pass">phone</label>
                        <input type="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" id="pass" value="{{ old('phone') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="pass">Desc/Note</label>
                        <textarea name="desc" class="form-control @error('desc') is-invalid @enderror">
                            {{ old('desc') }}
                        </textarea> 
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection