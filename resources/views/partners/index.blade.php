@extends('layouts.adminLayout')

@section('title')
Operator List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Partners</a></li>
    <li class="breadcrumb-item active">List</li>
@endsection

@section('content')
<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th class="" scope="col">Name</th>
                <th class="" scope="col">Phone</th>
                <th class="" scope="col">Email</th>
                <th class="text-center" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($partners as $partner)
            <tr style="backgroundColor:#fff">
                <td class="">{{$partner->name}}</td>
                <td class="">{{$partner->phone}}</td>
                <td class="">{{$partner->email}}</td>
                <td class=" text-center justify-content-center"> 
                    <a href="{{'partners/'.$partner->id}}" class="btn btn-info btn-sm text-light">View</a>
                    <a href="{{'partners/'.$partner->id.'/edit'}}" class="btn btn-success btn-sm text-light">Edit</a>
                    <form action="{{url('partners/'.$partner->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <div class="display-3 text-center">No partners Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection