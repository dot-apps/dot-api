@extends('layouts.userLayout')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('content')
<p><em>Welcome {{Auth::user()->name}}</em></p>
<p><em>resources/views/home.blade.php</em></p>
<div class="row justify-content-center">
    <div class="col-md-8">
        {{--Success Msg--}}
        @if (session('msg_success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{session('msg_success')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif            
    </div>
</div>
@endsection
