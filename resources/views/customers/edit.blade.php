@extends('layouts.adminLayout')

@section('title')
Edit Customer - {{ $customer->name }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/customers">Customers</a></li>
    <li class="breadcrumb-item active">Edit Customer</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Edit Customer</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{url('customers/' . $customer->id)}}" class="row" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group form-group-default required">
                        <label for="full_name">Type</label>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio1" value="1" {{ ($customer->type == 1) ? 'checked' : ''}}>
                            <label for="radio1">Personal</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio2" value="2" {{ $customer->type == 2 ? 'checked' : ''}}>
                            <label for="radio2">Corporatte</label>
                        </div>
                    </div>
                    <div class="card-title">Detail Pacakge</div>
                        <div class="form-group form-group-default required">
                        <label for="full_name">Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ $customer->name }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Phone</label>
                        <input type="text" name="phone" min=1000 class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ $customer->phone }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Email</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="Email" value="{{ $customer->email }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Address</label>
                        <textarea name="address" id="address" class="form-control" cols="30" rows="10">{{ $customer->address}}</textarea>
                    </div>
                    <input type="hidden" name="_method" value="PUT">
                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
