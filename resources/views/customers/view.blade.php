@extends('layouts.adminLayout')

@section('title')
Detail Package - {{ $package->name }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/packages">Packages</a></li>
    <li class="breadcrumb-item active">Package Detail</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title"></div>
            </div>
            <div class="card-body">
                <h3 class="text-center">{{$package->name}}</h3>
                <p><strong>Price per Day: </strong> Rp.{{number_format($package->price_day)}}</p> 
                <p><strong>Price 7 Days: </strong> Rp.{{number_format($package->price_7days)}}</p> 
                <p><strong>Status: </strong> {{$package->is_active == 1 ? 'ACTIVE' : 'UN-ACTIVE' }}</p> 
            </div>
        </div>
    </div>
    <!-- <div class="col-md-2"></div> -->
    <div class=" col-md-8">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Products</div>
            </div>
            <div class="card-body">
                <table class="table ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $no= 1?>
                    @forelse ($packageProducts as $pacProd)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$pacProd->name}}</td>
                            <td>{{$pacProd->qty}}</td>
                        </tr>
                        <?php $no++?>
                    @empty
                        <p>No Products Available</p>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection