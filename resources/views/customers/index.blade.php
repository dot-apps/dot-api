@extends('layouts.adminLayout')

@section('title')
Package List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Customers</a></li>
    <li class="breadcrumb-item active">Customer List</li>
@endsection

@section('content')
<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th class="" scope="col">Name</th>
                <th class="" scope="col">Type</th>
                <th class="" scope="col">Phone</th>
                <th class="" scope="col">Address</th>
                <th class="" scope="col">Email</th>
                <th class="text-center" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($customers as $customer)
            <tr style="backgroundColor:#fff">
                <td class="">{{$customer->name}}</td>
                <td class="">{{$customer->type == 1 ? "Personal" : "Corporate"}}</td>
                <td class="">{{$customer->phone}}</td>
                <td class="">{{$customer->address}}</td>
                <td class="">{{$customer->email}}</td>
                <td class=" text-center justify-content-center"> 
                    <!-- <a href="{{'customers/'.$customer->id}}" class="btn btn-info btn-sm text-light">View</a> -->
                    <a href="{{'customers/'.$customer->id.'/edit'}}" class="btn btn-success btn-sm text-light">Edit</a>
                    <form action="{{url('customers/'.$customer->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <div class="display-3 text-center">No customers Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection