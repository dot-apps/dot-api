@extends('layouts.adminLayout')

@section('title')
User List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Master User</a></li>
    <li class="breadcrumb-item active">User List</li>
@endsection

@section('content')
<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th class="" scope="col">Name</th>
                <th class="" scope="col">Email</th>
                <th class="" scope="col">Role</th>
                <!-- <th scope="col">Gender</th> -->
                <!-- <th scope="col">Birth Date</th> -->
                <!-- <th scope="col">Country</th> -->
                <th class="text-center" scope="col">Created At</th>
                <!-- <th scope="col">Updated At</th> -->
                <th class="text-center" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($users as $user)

            <tr style="backgroundColor:#fff">
                <td class="">
                    <a href="{{'users/'.$user->id}}">{{$user->name}}</a>
                </td>
                <td class="">{{$user->email}}</td>
                <!-- <td>{{$user->gender}}</td> -->
                <!-- <td>{{($user->birth_date)? $user->birth_date : 'empty'}}</td> -->
                <!-- <td>{{($user->country)? $user->country : 'empty'}}</td> -->
                <td class="">{{$user->role}}</td>
                <td class="text-center">{{$user->created_at}}</td>
                <!-- <td>{{$user->updated_at}}</td>   -->
                <td class="justify-content-center text-center">
                    {{-- <a href="{{'users/'.$user->id}}" class="btn btn-info btn-sm text-light"><small>View</small></a> --}}
                    <a href="{{'users/edit/'.$user->id}}" class="btn btn-success btn-sm text-light"><small>Edit</small></a>
                    <form action="{{url('users/'.$user->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        {{-- <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete"> --}}
                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light"><small>Delete</small></button>
                    </form>
                </td>
            </tr>
        @empty
            <div class="display-5 text-center">
                <b>No Users Available</b>
            </div>
        @endforelse
        </tbody>
    </table>

</div>
{{ $users->links() }}
@endsection
