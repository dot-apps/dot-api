@extends('layouts.adminLayout')

@section('title')
New User
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Master User</a></li>
    <li class="breadcrumb-item active">New User</li>
@endsection

@section('content')
<div class="row">
    {{-- <div class="offset-md-3 col-md-6"> --}}
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add new user</div>
            </div>
            <div class="card-body">
                <form method="POST" action="/users/store" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group form-group-default required">
                        <label class="" for="full_name">Full Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name">
                        @error('name')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group form-group-default required">
                        <label class="" for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email">
                        @error('email')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group form-group-default required">
                        <label class="" for="pass">Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="pass">
                        @error('password')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group form-group-default required">
                        <label class="" for="pass">Gender</label>
                        <div class="form-check form-check-inline success">
                            <input type="radio" id="male" name="gender" value="M" class="" required />
                            <label for="male">Male</label>
                        </div>
                        <div class="form-check form-check-inline success">
                            <input type="radio" id="female" name="gender" value="F" class="custom-control-input" required />
                            <label for="female">Female</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {{-- <label class="" for="role">Role</label> --}}
                        <select class="full-width" name="role" class="form-control @error('role') is-invalid @enderror" id="role" data-init-plugin="select2">
                            <option value="none" disabled selected>Select role</option>
                            <option value="warehouse">Warehouse</option>
                            <option value="sales">Sales</option>
                            <option value="finance">Finance</option>
                        </select>
                        @error('role')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    {{-- <div class="form-group">
                        <label for="full_name">Full Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name">
                        @error('name')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email">
                        @error('email')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pass">Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="pass">
                        @error('password')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div> --}}
                    {{-- <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="male" name="gender" value="M" class="custom-control-input" />
                            <label class="custom-control-label" for="male">Male</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="female" name="gender" value="F" class="custom-control-input" />
                            <label class="custom-control-label" for="female">Female</label>
                        </div>
                    </div> --}}
                    <!-- <div class="form-group">
                        <label for="birth_date">Birth Date</label>
                        <input type="date" name="birth_date" class="form-control" id="birth_date">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" name="country" class="form-control" id="country">
                    </div> -->
                    {{-- <div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" class="form-control @error('role') is-invalid @enderror" id="role">
                            <option value="warehouse">Warehouse</option>
                            <option value="sales">Sales</option>
                            <option value="finance">Finance</option>
                        </select>
                        @error('role')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div> --}}
                    <!-- <div class="form-group">
                        <label for="permissionSelect">Permission</label>
                        <select class="form-control" name="permission" id="permissionSelect">
                            <option value="read">Read</option>
                            <option value="write">Write</option>
                            <option value="execute">Execute</option>
                            <option value="delete">Delete</option>
                        </select>
                    </div> -->
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image">
                            <label class="custom-file-label" for="image">Upload Image</label>
                        </div>
                    </div>
                    <div class="form-group mt-4 mb-0">
                        <button type="submit" class="btn bt-lg btn-primary btn-block">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
