<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		@if($transaction->type == 1)
		<title>PEMINJAMAN KE REKANAN</title>
		@else
		<title>PENGEMBALIAN KE REKANAN</title>
		@endif
	</head>
	<body style="font-family: 'Poppins', sans-serif; margin-top: 2cm; margin-bottom: 0.75cm; font-size: 11px;">
		<header style="position: fixed; top: -1.2cm; left: -1.25cm; right: -1.25cm; height: 1.14cm; width: 100%">
			<img src="{{ asset('/img/letterhead/header.svg') }}" width="100%">
			<img src="{{ asset('/img/letterhead/logo.svg') }}" style="width: 90px; margin-left: 1.25cm">
		</header>
		<footer style="position: fixed; bottom: -1.425cm; left: -1.25cm; right: -1.25cm; height: 1.75cm;">
			<p class="text-footer" style="color: #2851a4; color: #2851a4; margin-top: 0; text-align: center; margin-bottom: 5px; font-size: 12px;">Jl. Jeruk Raya &middot; Ruko Soho Jagakarsa No.9B Jakarta Selatan, 12620 &middot; 0857 1168 7748 &middot; hello@dot-rental.com &middot; @dot_rent</p>	
			<img src="{{ asset('/img/letterhead/footer.svg') }}" width="100%">
		</footer>
		<main>
			<p style="text-align: center; margin-top: -.75cm; margin-bottom: 20px"><b>&nbsp;</b></p>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<tbody>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 18%; background-color: transparent; padding-top: 0; padding-bottom: 0; vertical-align: top;" width="18%" align="left" bgcolor="transparent">
							@if($transaction->type == 1)
							Tempat Sewa
							@else
							Telah diterima oleh
							@endif
						</td>
						<td style="padding: 5px; text-align: left; width: 1%; background-color: transparent; padding-top: 0; padding-bottom: 0; vertical-align: top;" width="1%" align="left" bgcolor="transparent">:</td>
						<td style="padding: 5px; text-align: left; width: 82%; background-color: transparent; padding-top: 0; padding-bottom: 0; vertical-align: top;" width="82%" align="left" bgcolor="transparent">
							{{ $partner->name }}
							<br>
							({{ $transaction->giver}})
						</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 18%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="18%" align="left" bgcolor="transparent">Alat/barang</td>
						<td style="padding: 5px; text-align: left; width: 1%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="1%" align="left" bgcolor="transparent">:</td>
						<td style="padding: 5px; text-align: left; width: 82%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="82%" align="left" bgcolor="transparent">
							@foreach(json_decode($transaction->products,TRUE) as $_prod)
                            {{ $_prod['qty'] }} x {{ $_prod['product_name'] }}
                            <br>
                            @endforeach
						</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 18%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="18%" align="left" bgcolor="transparent">Catatan</td>
						<td style="padding: 5px; text-align: left; width: 1%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="1%" align="left" bgcolor="transparent">:</td>
						<td style="padding: 5px; text-align: left; width: 82%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="82%" align="left" bgcolor="transparent">
							{{ $transaction->note }}
						</td>
					</tr>
				</tbody>
			</table>
			<div id="notices" style="">
				<div style="text-align: center; margin-bottom: 10px"><b>&nbsp;</b></div>
				<div>Jakarta, {{ $transaction->tanggal }}</div>
				<div>Jam : {{ $transaction->jam }}</div>
				<div>Code : {{ strtoupper($transaction->code) }}</div>
			</div>
			<div id="notices">
				<div style="text-align: center; margin-bottom: 10px"><b>&nbsp;</b></div>
				<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 30px;" width="100%">
					<tbody>
						<tr>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"><b>Yang Menyerahkan</b></td>
							<td style="padding: 5px; background-color: transparent; width: 40%; text-align: center;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"><b>Yang Menerima</b></td>
						</tr>
						<tr>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; height: .75cm;" width="30%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 40%; text-align: center; height: .75cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; height: .75cm;" width="30%" bgcolor="transparent" align="center"></td>
						</tr>
						<tr>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">{{ $transaction->giver }}</td>
							<td style="padding: 5px; background-color: transparent; width: 40%; text-align: center;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">{{ $transaction->receiver }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</main>
 	</body>
</html>