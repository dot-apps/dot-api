<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>SURAT JALAN KELUAR</title>
	</head>
	<body style="font-family: 'Poppins', sans-serif; margin-top: 2cm; margin-bottom: 0.75cm; font-size: 11px;">
		<header style="position: fixed; top: -1.2cm; left: -1.25cm; right: -1.25cm; height: 1.14cm; width: 100%">
			<img src="{{ asset('/img/letterhead/header.svg') }}" width="100%">
			<img src="{{ asset('/img/letterhead/logo.svg') }}" style="width: 90px; margin-left: 1.25cm">
		</header>
		<footer style="position: fixed; bottom: -1.425cm; left: -1.25cm; right: -1.25cm; height: 1.75cm;">
			<p class="text-footer" style="color: #2851a4; color: #2851a4; margin-top: 0; text-align: center; margin-bottom: 5px; font-size: 12px;">Jl. Jeruk Raya &middot; Ruko Soho Jagakarsa No.9B Jakarta Selatan, 12620 &middot; 0857 1168 7748 &middot; hello@dot-rental.com &middot; @dot_rent</p>	
			<img src="{{ asset('/img/letterhead/footer.svg') }}" width="100%">
		</footer>
		<main>
			<p style="text-align: center; margin-top: -.75cm; margin-bottom: 20px">
				@if($inout->type == 1)
				<b>SURAT JALAN ALAT KELUAR</b>
				@else
				<b>SURAT JALAN ALAT KEMBALI</b>
				@endif
			</p>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<tbody>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 15%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="15%" align="left" bgcolor="transparent">Tanggal</td>
						<td style="padding: 5px; text-align: left; width: 90%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: {{ $tanggal }}</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 15%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="15%" align="left" bgcolor="transparent">Nama Penyewa </td>
						<td style="padding: 5px; text-align: left; width: 90%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: {{ $customer['customer_name'] }}</td>
					</tr>
				</tbody>
			</table>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<thead>
					<tr>
						<th class="desc" style="padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal; text-align: center;" align="center">NO</th>
						<th class="" style="text-align: left; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">PERALATAN</th>
						<th style="text-align: center; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">JUMLAH UNIT</th>
						<th style="text-align: center; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">NO. SERI</th>
						<th style="text-align: left; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">KETERANGAN</th>
					</tr>
				</thead>
				<tbody>
					@php $no=1 @endphp
					@foreach($inout_items as $ii)
					<tr>
						<td class="desc" style="padding: 5px; border-bottom: 1px solid #C1CED9; text-align: center; vertical-align: top;" align="left" valign="top">{{ $no }}</td>
						<td class="qty" style="padding: 5px; text-align: left; border-bottom: 1px solid #C1CED9; vertical-align: top;" align="right" valign="top">
							{{ $ii->product_name}}
						</td>
						<td class="qty" style="padding: 5px; text-align: right; border-bottom: 1px solid #C1CED9; vertical-align: top;" align="right" valign="top">
							{{ $ii->jml}}
						</td>
						<td class="qty" style="padding: 5px; text-align: center; border-bottom: 1px solid #C1CED9; vertical-align: top;" align="center" valign="top">
							@php
							$serials = explode(',',$ii->serial_no);
							
							@endphp
							@foreach($serials as $_item)
							{{ $_item }}
							<br>
							@endforeach
						</td>
						<td class="total" style="padding: 5px; text-align: left; border-bottom: 1px solid #C1CED9; vertical-align: top;" align="right" valign="top">
							
						</td>
					</tr>
					@php $no++ @endphp
					@endforeach
				</tbody>
			</table>
			<div id="notices">
				<div style="text-align: center; margin-bottom: 10px">
					@if($inout->type == 1)
					<b>PENGAMBILAN</b>
					@else
					<b>PEMULANGAN</b>
					@endif
				</div>
				<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 30px;" width="100%">
					<tbody>
						<tr>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"><b>Menyerahkan</b></td>
							<td style="padding: 5px; background-color: transparent; width: 40%; text-align: center; height: .15cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"><b>Penerima</b></td>
						</tr>
						<tr>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">
								
							</td>
							<td style="padding: 5px; background-color: transparent; width: 40%; text-align: center; height: .75cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"></td>
						</tr>
						<tr>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">{{ $inout->given_by }}</td>
							<td style="padding: 5px; background-color: transparent; width: 40%; text-align: center; height: .15cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">{{ $inout->receiver_by }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</main>
 	</body>
</html>