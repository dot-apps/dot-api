@extends('layouts.adminLayout')

@section('title')
Inventorie Partner List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href='/inventories'>Inventories</a></li>
    <li class="breadcrumb-item active">Partner</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">
                    Inventory Partner
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead class="">
                        <tr >
                            <th  scope="col">#</th>
                            <th  scope="col">Name</th>
                            <th  scope="col">Qty. Item</th>
                            <!-- <th  scope="col"></th> -->
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $no=1 @endphp
                    @forelse ($list as $_list)
                        <tr style="backgroundColor:#fff">
                            <td>{{ $no }}</td>
                            <td>{{ $_list->name }}</td>
                            <td>{{ ($_list->qty =='') ? '-' : $_list->qty  }}</td>
                            <td class="justify-content-center">
                                <a href="{{'partner/'.$_list->id}}" class="btn btn-info btn-sm text-light">View</a>
                            </td>
                        </tr>
                        @php $no++ @endphp
                    @empty
                        <div class="display-3 text-center">No transactions Available</div>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
