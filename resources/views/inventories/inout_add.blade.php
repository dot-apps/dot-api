@extends('layouts.adminLayout')
@section('title')
Add New In Out 
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/inventories">Inventories</a></li>
    <li class="breadcrumb-item"><a href="/inventories/inout">In Out</a></li>
    <li class="breadcrumb-item active">Add New<li>
@endsection
<style>
    .form-group {
        margin-bottom:unset !important;
    }
</style>
@section('content')
<div class="row">
    <div class=" col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group form-group-default required" style="margin-bottom:20px !important;height:70px">
                    <label for="full_name">Invoice Number</label>
                    <input type="text" name="invoice_number" class="form-control @error('serial_number') is-invalid @enderror" id="invoice_number" value="{{ $no }}" required>
                </div>
                <a href="" type="submit" class="btn btn-primary btn-block" id="btnSearch">Search</a>
            </div>
        </div>
    </div>

    @isset($transaction)
    <div class=" col-md-8">
        <div id="card-linear-color" class="card card-default">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group form-group-default required" style="margin-bottom:unset;height: 118px">
                    <label for="full_name">Input Barcode</label>
                    <input style="font-size: 43px;height:60px" type="text" name="barcode_search" class="form-control @error('barcode') is-invalid @enderror" autofocus>
                </div>
            </div>
        </div>
    </div>
    @endisset

    @if (isset($transaction))
    <div class=" col-md-12">
        <form action="/inventories/inout" method="POST" class="row">
            @csrf
            <input type="hidden" name="invoice_number" value="{{ $no }}" required>
            <div id="card-linear-color" class="card card-default">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th >Equiepment</th>
                                <th>Qty</th>
                                <th>Day</th>
                                <th>Dates</th>
                                <th>Barcode</th>
                                <th>Serial Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no=1 @endphp
                            @foreach ($products as $key=>$_product)
                            <tr>
                                <td scope="row">
                                    {{ $no }}
                                </td>
                                <td>
                                    {{ $_product->id }}
                                    {!! $_product->name !!}
                                </td>
                                <td>
                                    {!! $_product->qty !!}
                                </td>
                                <td>
                                    {!! $_product->day !!}
                                </td>
                                <td>
                                    <div class="form-group form-inline">
                                        <input type="date" name="start_date[1][{{ $_product->cart_id }}][{{ $_product->id }}]" class="form-control" id="" aria-describedby="helpId" placeholder="">
                                        &nbsp to &nbsp
                                        <input type="date" name="end_date[1][{{ $_product->cart_id }}][{{ $_product->id }}]" class="form-control" id="" aria-describedby="helpId" placeholder="">
                                    </div>
                                </td>
                                <td>
                                    @for($i = 1;$i <= $_product->qty;$i++)
                                    <div class="form-group form-group-default">
                                        <label for="full_name">{{ $i }}</label>

                                        <input type="text" name="barcode[1][{{ $_product->cart_id }}][{{ $_product->id }}][{{ $i }}]" class="form-control barcode-prod-{{ $_product->id }} @error('serial_number') is-invalid @enderror" >

                                        <input type="hidden" name="product_name[1][{{ $_product->cart_id }}][{{ $_product->id }}][{{ $i }}]" value="{!! $_product->name !!}" >
                                    </div>
                                    @endfor
                                </td>
                                <td>
                                    @for($i = 1;$i <= $_product->qty;$i++)
                                    <div class="form-group form-group-default">
                                        <label for="full_name">{{ $i }}</label>
                                        <input type="text" name="serial_number[1][{{ $_product->cart_id }}][{{ $_product->id }}][{{ $i }}]" class="form-control serial-prod-{{ $_product->id }}  @error('serial_number') is-invalid @enderror" >
                                    </div>
                                    @endfor
                                </td>
                                @php $no++ @endphp
                            </tr>
                            @endforeach
                            @php
                                $package_id = 0
                            @endphp
                            @foreach ($packages as $key => $_package)
                                @if ($package_id != $_package->id)
                                    @php $package_id = $_package->id @endphp
                                    <tr>
                                        <td scope="row" colspan="2">
                                            <b>{{ strtoupper($_package->name) }}</b>
                                        </td>
                                        <td>
                                            {{ $_package->qty }}
                                        </td>
                                        <td>
                                            {{ $_package->day }}
                                        </td>
                                        <td>
                                            <div class="form-group form-inline">
                                                <input type="date" name="start_date[2][{{ $_package->cart_id }}][{{ $_package->id }}]" class="form-control" id="" aria-describedby="helpId" placeholder="">
                                                &nbsp to &nbsp
                                                <input type="date" name="end_date[2][{{ $_package->cart_id }}][{{ $_package->id }}]" class="form-control" id="" aria-describedby="helpId" placeholder="">
                                            </div>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                                <tr>
                                    <td scope="row">
                                        {{ $no }}
                                    </td>
                                    <td>
                                        {{ $_package->product_id }}
                                        {!! $_package->product_name !!}
                                        <br>
                                        ({!! $_package->product_qty !!} X  {{ $_package->qty }})
                                    </td>
                                    <td>
                                        {!! $_package->product_qty !!}
                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        {{-- {{ $_package->product_qty }} --}}
                                        @for($i = 1;$i <= ($_package->qty*$_package->product_qty);$i++)
                                        <div class="form-group form-group-default">
                                            <label for="full_name">{{ $i }}</label>
                                            
                                            <input type="text" name="barcode[2][{{ $_package->cart_id }}][{{ $_package->id }}][{{ $_package->product_id }}][{{ $i }}]" class="form-control barcode-prod-{{ $_package->product_id }}" >

                                            <input type="hidden" name="product_name[2][{{ $_package->cart_id }}][{{ $_package->id }}][{{ $_package->product_id }}][{{ $i }}]" value="{!! $_package->product_name !!}">
                                        </div>
                                        @endfor
                                    </td>
                                    <td>
                                        {{-- {{ $_package->product_qty }} --}}
                                        @for($i = 1;$i <= ($_package->qty*$_package->product_qty);$i++)
                                        <div class="form-group form-group-default">
                                            <label for="full_name">{{ $i }}</label>
                                            <input type="text" name="serial_number[2][{{ $_package->cart_id }}][{{ $_package->id }}][{{ $_package->product_id }}][{{ $i }}]" class="form-control serial-prod-{{ $_package->product_id }}" >
                                        </div>
                                        @endfor
                                    </td>
                                    @php $no++ @endphp
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">SAVE</button>
        </form>
    </div>
    @endif
    
</div>
<script>
    $('input[name=invoice_number]').on('change',function(){
        $('a').attr('href','?no='+$(this).val())
    })

    var typingTimer;                //timer identifier
    var doneTypingInterval = 200;  //time in ms, 5 second for example
    var $input = $('input[name=barcode_search]');
    //on keyup, start the countdown
    $input.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
    clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
        var barcode = $input.val()
        if(barcode == '') {
            return false
        }
        var url = "{{ url('items/find?res_type=json') }}"+"&barcode="+barcode
        $.get(url, function( data ) {
            if(data.length==0) {
                alert('Item no found')
            }
            else{
                var item  = data[0]
                var productId = item.product_id
                $.each($('.barcode-prod-'+productId), function () {
                    if($(this).val() == '') {
                        $(this).val(item.barcode)
                        $.each($('.serial-prod-'+productId), function () {
                            console.log($(this).val())
                            if($(this).val() == '') {
                                $(this).val(item.serial_number)
                                return false
                            }
                        });
                        return false                        
                    }
                    else if($(this).val() == barcode) {
                        alert('Already Input')
                        return false
                    }
                })
            }
        });
        $input.val('')
    }
</script>
@endsection
