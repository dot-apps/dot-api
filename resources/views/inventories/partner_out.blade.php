@extends('layouts.adminLayout')
@section('title')
Add New In Out 
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/inventories">Inventories</a></li>
    <li class="breadcrumb-item"><a href="/inventories/partner/{{ $partners->id }}">Partner</a></li>
    <li class="breadcrumb-item active">Add New Out<li>
@endsection
@section('content')
<div class="row">
    <div class=" col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add New Out Item</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group form-group-default required">
                    <label for="full_name">Partner</label>
                    <select id="partners" class="form-control" required="required" readonly>
                        <option value="{{ $partners->id }}">{{ $partners->name }}</option>
                    </select>
                </div>
                <div class="form-group form-group-default required">
                    <label for="full_name">Barcode</label>
                    <input style="font-size: 43px;height:60px" type="text" name="barcode_search" class="form-control @error('barcode') is-invalid @enderror" autofocus>
                </div>
            </div>
        </div>
    </div>
    <div class=" col-md-8">
        <div id="card-linier-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">List Items Partner : <span id="partnerName"></span></div>
            </div>
            <div class="card-body">
                <form method="POST" action="/inventories/partner" enctype="multipart/form-data">
                    @csrf
                    <input type="text" class="hidden" name="partner_id" value="" >
                    <input type="text" class="hidden" name="type" value="2" >
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-group-default required">
                                <label for="full_name">Giver</label>
                                <input type="text" name="giver" class="form-control @error('glyphicon-remove-circle') is-invalid @enderror" value="{{ old('giver') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-default required">
                                <label for="full_name">Receiver</label>
                                <input type="text" name="receiver"  class="form-control @error('glyphicon-remove-circle') is-invalid @enderror" value="{{ old('giver') }}" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <table class="table " id="tbl">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Serial Number</th>
                                        <th>Barcode</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tblList">
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <div class="form-group form-group-default required">
                                <label for="full_name">Notes</label>
                                <textarea name="note"  class="form-control" style="height: 15%" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var typingTimer;                //timer identifier
    var doneTypingInterval = 200;  //time in ms, 5 second for example
    var $input = $('input[name=barcode_search]');
    //on keyup, start the countdown
    $input.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
    clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
        var barcode = $input.val()
        if(barcode == '') {
            return false
        }
        var url = "{{ url('items/find?res_type=json') }}"+"&barcode="+barcode
        $.get(url, function( data ) {
            if(data.length==0) {
                alert('Item no found')
            }
            else{
                var item  = data[0]
                var productId = item.product_id

                console.log(item)
                    
                var lastIndex = $('#tbl tr').length;
                var nextIndex = lastIndex++;

                $('#partners').attr('disabled', 'disabled')
                $('#partnerName').html($('#partners option:selected').text())
                $('input[name=partner_id]').val($('#partners').val())
                var html='<tr>'+
                '   <td>'+
                '       <input type="text" name="product_id['+item.product_id+']" value="'+item.product_id+'"  class="hidden" readonly/>'+
                '       <input type="text" name="product_name['+item.product_id+']" value="'+item.product_name+'" class="hidden" readonly />'+
                        item.product_name+
                '   </td>'+
                '   <td>'+
                '       <input type="text" name="product_serial_number['+item.product_id+'][]" value="'+item.serial_number+'" class="hidden" readonly />'+
                        item.serial_number+
                '   </td>'+
                '   <td>'+
                '       <input type="text" name="product_barcode['+item.product_id+'][]" value="'+item.barcode+'" class="hidden" readonly />'+
                        item.barcode+
                '   </td>'+
                '   <td><a href="#" class="btn btn-danger btn-remove-row">REMOVE</a></td>'+
                '  </tr>'

                $('#tblList').append(html)
                $('#products').val('')
                $('#qty').val('')
                $('#serials').val('')
                $('#desc').val('')

            }
        });
        $input.val('')
    }

    $('#tblList').on('click','td .btn-remove-row',function(e){
        e.preventDefault();
        $(this).parents('tr').remove();
    });
</script>
@endsection
