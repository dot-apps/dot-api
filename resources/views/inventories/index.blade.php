@extends('layouts.adminLayout')

@section('title')
Inventory In Out
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Inventory</a></li>
    <li class="breadcrumb-item active">Inventory In Out</li>
@endsection

@section('content')
<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr >
                <th  scope="col" rowspan="2">#</th>
                <th scope="col" rowspan="2" style="width:90%">Product</th>
                <th scope="col" colspan="4" class="text-center">Stock</th>
                <th scope="col" rowspan="2" style="width:10%" class="text-center">Action</th>
            </tr>
            <tr>
                <th class="text-center">Total</th>
                <th class="text-center">Avail</th>
                <th class="text-center">On Rent</th>
                <th class="text-center">Broken</th>
            </tr>
        </thead>
        <tbody>
        @php $no=1 @endphp
        @forelse ($list as $_list)
            <tr style="backgroundColor:#fff">
                <td>{{$no}}</td>
                <td>{{ $_list->name }}</td>
                <td class="text-center">{{ ($_list->jml == '') ? 0 : $_list->jml }}</td>
                <td class="text-center">{{ ($_list->avail == '') ? 0 : $_list->avail }}</td>
                <td class="text-center">{{ ($_list->on_rent == '') ? 0 : $_list->on_rent }}</td>
                <td class="text-center">{{ ($_list->broken == '') ? 0 : $_list->broken }}</td>
                <td class="text-center">
                    <a href="{{'inventories/'.$_list->id}}" class="btn btn-info btn-sm text-light">View</a>
                </td>
            </tr>
            @php $no++ @endphp
        @empty
            <div class="display-3 text-center">No transactions Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection
