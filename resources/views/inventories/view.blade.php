@extends('layouts.adminLayout')

@section('title')
Detail Stock - {{ $product->name }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/inventories">Inventories</a></li>
    <li class="breadcrumb-item active">Inventory Detail</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add New Item</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="/items" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}" required>
                    <input type="hidden" name="is_misc" value="{{ $product->is_misc }}" required>
                    <input type="hidden" name="barcode" value="{{ $product->barcode }}" required>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Serial Number</label>
                        <input type="text" name="serial_number" class="form-control @error('serial_number') is-invalid @enderror" id="full_name" value="{{ old('serial_number') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Status</label>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="status" id="radio1" value="1" checked>
                            <label for="radio1">Avail</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="status" id="radio2" value="2">
                            <label for="radio2">On Rent</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="status" id="radio2" value="3">
                            <label for="radio2">Broken</label>
                        </div>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Purchased At</label>
                        <input type="date" name="purchase_at"  class="form-control @error('purchase_at') is-invalid @enderror" id="phone" value="{{ old('purchase_at') }}" required>
                    </div>
                    @if($product->is_misc == 1)
                    <div class="form-group form-group-default required">
                        <label for="full_name">Quantity</label>
                        <input type="number" min="1" name="quantity"  class="form-control @error('quantity') is-invalid @enderror" value="{{ old('quantity') }}" required>
                    </div>
                    @endif
                    <div class="form-group form-group-default required">
                        <label for="full_name">Description</label>
                        <textarea name="desc" id="address" class="form-control" cols="30" rows="10">{{ old('address')}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
    <div class=" col-md-8">
        <div id="card-linier-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">{{ $product->name }}</div>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active" data-toggle="tab" role="tab" data-target="#tab2hellowWorld" href="#" aria-selected="true">In House</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab" data-target="#tab2FollowUs" class="" aria-selected="false">Partner</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2hellowWorld">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Barcode</th>
                                            <th>Serial Number</th>
                                            <th>Description</th>
                                            <th>Purchase At</th>
                                            <th>Added At</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no= 1?>
                                    @forelse ($items as $_item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $_item->barcode }}</td>
                                            <td>{{ $_item->serial_number }}</td>
                                            <td>{{ $_item->desc }}</td>
                                            <td>{{ $_item->purchase_at }}</td>
                                            <td>{{ $_item->created_at }}</td>
                                            <td class="text-center">
                                                @if ($_item->status == 1)
                                                    <button type="button" class="btn btn-success">Avail</button>
                                                @elseif($_item->status == 2)
                                                    <button type="button" class="btn btn-primary">On Rent</button>
                                                @else
                                                    <button type="button" class="btn btn-danger">Broke</button>
                                                @endif
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-edit" data-items="{{ json_encode($_item) }}" data-toggle="modal" data-target="#mdlEdit">Edit</button>
                                            </td>
                                        </tr>
                                        <?php $no++?>
                                    @empty
                                        <p>No Items</p>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2FollowUs">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Barcode</th>
                                            <th>Serial Number</th>
                                            <th>Description</th>
                                            <!-- <th>Purchase At</th> -->
                                            <th>Added At</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no= 1?>
                                    @forelse ($partner_items as $_item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $_item->barcode }}</td>
                                            <td>{{ $_item->serial_number }}</td>
                                            <td>{{ $_item->desc }}</td>
                                            <td>{{ $_item->purchase_at }}</td>
                                            <td>{{ $_item->created_at }}</td>
                                            <td class="text-center">
                                                @if ($_item->status == 1)
                                                    <button type="button" class="btn btn-success">Avail</button>
                                                @elseif($_item->status == 2)
                                                    <button type="button" class="btn btn-primary">On Rent</button>
                                                @else
                                                    <button type="button" class="btn btn-danger">Broke</button>
                                                @endif
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-edit" data-items="{{ json_encode($_item) }}" data-toggle="modal" data-target="#mdlEdit">Edit</button>
                                            </td>
                                        </tr>
                                        <?php $no++?>
                                    @empty
                                        <tr>
                                            <td colspan="8" class="text-center">No Items</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="mdlEdit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Status Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="product_id" required>
                    <input type="hidden" name="item_id" value="" required>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Serial Number</label>
                        <input type="text" name="serial_number" class="form-control @error('serial_number') is-invalid @enderror" id="full_name" value="{{ old('serial_number') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Status</label>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="status" id="radio1" value="1" checked>
                            <label for="radio1">Avail</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="status" id="radio2" value="2">
                            <label for="radio2">On Rent</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="status" id="radio2" value="3">
                            <label for="radio2">Broken</label>
                        </div>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Purchased At</label>
                        <input type="date" name="purchase_at"  class="form-control @error('purchase_at') is-invalid @enderror" id="phone" value="{{ old('purchase_at') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Description</label>
                        <textarea name="desc" id="address" class="form-control" cols="30" rows="10">{{ old('address')}}</textarea>
                    </div>
                    <input type="hidden" name="_method" value="PUT">
                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    $('.btn-edit').on('click',function(){
        var item = $(this).data('items')
        $('#mdlEdit').find('input[name=product_id]').val(item.product_id)
        $('#mdlEdit').find('input[name=id]').val(item.id)
        $('#mdlEdit').find('input[name=serial_number]').val(item.serial_number)
        $('#mdlEdit').find('input[name=status][value='+item.status+']').attr('checked','checked')
        $('#mdlEdit').find('input[name=purchase_at]').val(item.purchase_at)
        $('#mdlEdit').find('textarea[name=desc]').html(item.desc)
        $('#mdlEdit').find('form').attr('action','/items/'+item.id)
    })
</script>
@endsection
