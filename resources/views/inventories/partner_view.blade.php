@extends('layouts.adminLayout')
@section('title')
Add New In Out 
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/inventories">Inventories</a></li>
    <li class="breadcrumb-item"><a href="/inventories/inout">In Out</a></li>
    <li class="breadcrumb-item active">Detail<li>
@endsection
<style>
    .form-group {
        margin-bottom:unset !important;
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title font-weight-bold">
                    Partner Data
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width:20%">Name</th>
                            <th>:</th>
                            <th style="width:80%">
                                {{ $partner->name }}
                            </th>
                        </tr>
                        
                        <tr>
                            <th style="width:10%">Phone</th>
                            <th>:</th>
                            <th style="width:90%">
                                {{ $partner->phone }}
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title font-weight-bold">
                    Log
                </div>
                <a href="{{'in/'.$partner->id}}" class="btn btn-info btn-sm pull-right text-light">NEW IN</a>
                <a href="{{'out/'.$partner->id}}" class="btn btn-danger btn-sm pull-right text-light">NEW OUT</a>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Timestamp</th>
                            <th>IN/OUT</th>
                            <th >Code</th>
                            <th>Equiepments</th>
                            <th>Giver</th>
                            <th>Receiver</th>
                            <th>Note</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $_trans)
                        <tr>
                            <td>{{ $_trans->created_at }}</td>
                            <td>
                                {{ ($_trans->type == 1) ? 'IN' : 'OUT'}}
                            </td>
                            <td>{{ strtoupper($_trans->code) }}</td>
                            <td>
                                @foreach(json_decode($_trans->products,TRUE) as $_prod)
                                {{ $_prod['product_name'] }} x {{ $_prod['qty'] }}
                                <br>
                                @endforeach
                            </td>
                            <td>{{ $_trans->giver }}</td>
                            <td>{{ $_trans->receiver }}</td>
                            <td>{{ $_trans->note }}</td>
                            <td>
                                <a href="{{'unduh/'.$_trans->id}}" class="btn btn-info btn-sm text-light">PDF</a>
                                <form action="{{url('inventories/partner/'.$partner->id.'/'.$_trans->id)}}" method="POST" style="display:inline-block">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
