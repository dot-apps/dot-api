@extends('layouts.adminLayout')
@section('title')
Add New In Out 
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/inventories">Inventories</a></li>
    <li class="breadcrumb-item"><a href="/inventories/partner/{{ $partners->id }}">Partner</a></li>
    <li class="breadcrumb-item active">Add New In<li>
@endsection
@section('content')
<div class="row">
    <div class=" col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add New In Item</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group form-group-default required">
                    <label for="full_name">Partner</label>
                    <select id="partners" class="form-control" required="required" readonly>
                        <option value="{{ $partners->id }}">{{ $partners->name }}</option>
                    </select>
                </div>
                <div class="form-group form-group-default required">
                    <label for="full_name">Product</label>
                    <select id="products" class="form-control" required="required">
                        <option value="" disabled selected>Select a product</option>
                        @foreach($products as $_product)
                        <option value="{{ $_product->id }}">{{ $_product->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group form-group-default required">
                    <label for="full_name">Quantity</label>
                    <input type="number" id="qty" class="form-control @error('qty') is-invalid @enderror" value="{{ old('qty') }}" required>
                </div>
                <div class="form-group form-group-default required">
                    <label for="full_name">Price</label>
                    <input type="number" id="price" class="form-control @error('glyphicon-remove-circle') is-invalid @enderror" value="{{ old('price') }}" required>
                </div>
                <div class="form-group form-group-default required">
                    <label for="full_name">Serial Number</label>
                    <textarea id="serials" class="form-control" style="height: 15%" rows="5"></textarea>
                    <small><i>(one serial number per line)</i></small>
                </div>
                <div class="form-group form-group-default required">
                    <label for="full_name">Description</label>
                    <textarea id="desc" class="form-control" cols="30" rows="10">{{ old('address')}}</textarea>
                </div>
                <a class="btn btn-primary btn-block text-light" id="btnAdd">Submit</a>
            </div>
        </div>
    </div>
    <div class=" col-md-8">
        <div id="card-linier-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">List Items Partner : <span id="partnerName"></span></div>
            </div>
            <div class="card-body">
                <form method="POST" action="/inventories/partner" enctype="multipart/form-data">
                    @csrf
                    <input type="text" class="hidden" name="partner_id" value="" >
                    <input type="text" class="hidden" name="type" value="1" >
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-group-default required">
                                <label for="full_name">Giver</label>
                                <input type="text" name="giver" class="form-control @error('glyphicon-remove-circle') is-invalid @enderror" value="{{ old('giver') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-default required">
                                <label for="full_name">Receiver</label>
                                <input type="text" name="receiver"  class="form-control @error('glyphicon-remove-circle') is-invalid @enderror" value="{{ old('giver') }}" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <table class="table " id="tbl">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Serial Number</th>
                                        <th>Description</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tblList">
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <div class="form-group form-group-default required">
                                <label for="full_name">Notes</label>
                                <textarea name="note"  class="form-control" style="height: 15%" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#btnAdd').on('click',  function(event) {
        var lastIndex = $('#tbl tr').length;
        var nextIndex = lastIndex++;

        $('#partners').attr('disabled', 'disabled')
        $('#partnerName').html($('#partners option:selected').text())
        $('input[name=partner_id]').val($('#partners').val())
        var html='<tr>'+
        '   <td>'+
        '       <input type="text" name="product_id['+nextIndex+']" value="'+$('#products option:selected').val()+'"  class="hidden" readonly/>'+
        '       <input type="text" name="product_name['+nextIndex+']" value="'+$('#products option:selected').text()+'" class="hidden" readonly />'+
            $('#products option:selected').text()+
        '   </td>'

        html+='<td>'+
        '       <input type="text" name="product_qty['+nextIndex+']" value="'+$('#qty').val()+'" class="hidden" readonly />'+
        $('#qty').val()+
        '</td>'+
        '<td>'+
        '       <input type="text" name="product_price['+nextIndex+']" value="'+$('#price').val()+'" class="hidden" readonly />'+
        $('#price').val()+
        '</td>'
        
        html +='<td>'
        var serials = $('#serials').val().split("\n")
        var itemIndex = 1;
        serials.map(function(val, key) {
            html+='<input type="text" name="product_item['+nextIndex+']['+itemIndex+']" value="'+val+'" class="hidden" readonly />'
            html+=val
            html+='<br>'
            itemIndex++
        })
        html +='</td>'

        html+='<td>'+
            $('#desc').val()+
        '   </td>'+
        '<td><a href="#" class="btn btn-danger btn-remove-row">REMOVE</a></td>'+
        '</tr>'

        $('#tblList').append(html)
        $('#products').val('')
        $('#qty').val('')
        $('#serials').val('')
        $('#desc').val('')

    });

    $('#tblList').on('click','td .btn-remove-row',function(e){
        e.preventDefault();
        $(this).parents('tr').remove();
    });
</script>
@endsection
