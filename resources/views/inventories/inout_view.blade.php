@extends('layouts.adminLayout')
@section('title')
Add New In Out 
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/inventories">Inventories</a></li>
    <li class="breadcrumb-item"><a href="/inventories/inout">In Out</a></li>
    <li class="breadcrumb-item active">Detail<li>
@endsection
<style>
    .form-group {
        margin-bottom:unset !important;
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title font-weight-bold">
                    Customer Data
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    @php
                        $customer = json_decode($transaction->customers,TRUE);
                    @endphp
                    <thead>
                        <tr>
                            <th style="width:20%">Name</th>
                            <th>:</th>
                            <th style="width:80%">
                                {{$customer['customer_name']}}
                            </th>
                        </tr>
                        
                        <tr>
                            <th style="width:10%">Phone</th>
                            <th>:</th>
                            <th style="width:90%">
                                {{$customer['customer_phone']}}
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title font-weight-bold">CART</div>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="tblCart">
                    <thead>
                        <tr>
                            <th style="width:50%">Paket</th>
                            <th style="width:50%">Deksripsi</th>
                            <th style="width:8%" class="text-center">Unit</th>
                            <th style="width:8%" class="text-center">Hari</th>
                            <th style="width:8%" class="text-center">Picked At</th>
                            <th style="width:8%" class="text-center">Return At</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($carts as $_cart)
                        <tr>
                            <td>
                                @if($_cart->type == 1)
                                    -
                                @else
                                    {{ $_cart->package_name }}
                                @endif
                            </td>
                            <td>{!! $_cart->product_name !!}</td>
                            <td class="text-center">{!!$_cart->qty !!}</td>
                            <td class="text-center">{!!$_cart->day !!}</td>
                            <td class="text-center">{!!$_cart->picked_at !!}</td>
                            <td class="text-center">{!!$_cart->return_at !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div> -->
    </div>
    <div class="col-md-12">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title font-weight-bold">IN/OUT ITEMS</div>
                <div class="card-title font-weight-bold pull-right">
                    <a  title="" class="btn btn-primary btn-sm" data-toggle="modal" href='#modalAdd'">ADD IN/OUT ITEMS</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="tblCart">
                    <thead>
                        <tr>
                            <th style="">Code</th>
                            <th style="">Date</th>
                            <th style="">Out/In</th>
                            <th style="">Giver</th>
                            <th style="">Receiver</th>
                            <th style="">Items</th>
                            <th style="">PDF</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    @foreach($inout as $_inout)
                        @php 
                            $items = json_decode($_inout->items,true);
                        @endphp
                        <tr>
                            <td>{{ $_inout->number }}</td>
                            <td>{{ $_inout->created_at }}</td>
                            <td class="text-center">{{ ($_inout->type == 1) ? 'OUT' : 'IN' }}</td>
                            <td class="text-center">{{ $_inout->given_by }}</td>
                            <td class="text-center">{{ $_inout->receiver_by }}</td>
                            
                            <td class="text-center">
                                <table class="table table-bordered">
                                    <tbody>
                                        @foreach($items as $_is)
                                        <tr>
                                            <td style="width: 60%">
                                                {{ $_is['product_name'] }}
                                            </td>
                                            <td style="width: 20%">
                                                {{ $_is['barcode'] }}
                                            </td>
                                            <td style="width: 20%">
                                                {{ $_is['serial_no'] }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <a href="{{'/inventories/inout/unduh/'.$transaction->id.'/'.$_inout->id}}" class="btn btn-info btn-sm text-light">
                                    PDF
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal left fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Pick Cart</h5>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="tblCart">
                    <thead>
                        <tr>
                            <th style="width:50%">Paket</th>
                            <th style="width:50%">Deksripsi</th>
                            <th style="width:8%" class="text-center">Unit</th>
                            <th style="width:8%" class="text-center">Hari</th>
                            <th style="width:8%" class="text-center">Pick</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($carts as $_cart)
                        @if($_cart->picked_at == '')
                        <tr>
                            <td>
                                @if($_cart->type == 1)
                                    -
                                @else
                                    {{ $_cart->package_name }}
                                @endif
                            </td>
                            <td>{!! $_cart->product_name !!}</td>
                            <td class="text-center">{!!$_cart->qty !!}</td>
                            <td class="text-center">{!!$_cart->day !!}</td>
                            <td class="text-center">
                                <input type="checkbox" class="pick-product" data-cart="{{ $_cart->cart_id }}" data-package="{{ $_cart->package_id }}" data-product="{{ $_cart->product_id }}" >
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a  title="" class="btn btn-primary" id="submitPickCart" data-toggle="modal" href='#modalAdd2'">
                    Submit
                </a>
            </div>
        </div>
    </div>
</div>

<div class="modal right fade" id="modalAdd2" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Add New</h5>
            </div>
            <div class="modal-body">
                <form action="/inventories/inout" method="post" accept-charset="utf-8">
                    @csrf
                    <input type="hidden" name="transaction_id" value="{{ $transaction->id }}" placeholder="">
                    <div class="form-group form-group-default required">
                        <label for="full_name">Type</label>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio1" value="1" checked>
                            <label for="radio1">Out</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio2" value="2">
                            <label for="radio2">In</label>
                        </div>
                    </div>
                    <br>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Given by</label>
                        <input style="" type="text" name="given_by" class="form-control @error('barcode') is-invalid @enderror" autofocus>
                    </div>
                    <br>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Receiver by</label>
                        <input style="" type="text" name="receiver_by" class="form-control @error('barcode') is-invalid @enderror" autofocus>
                    </div>
                    <br>
                    <h5 class="modal-title">Find</h5>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Input Barcode</label>
                        <input style="" type="text" name="barcode_search" class="form-control @error('barcode') is-invalid @enderror" autofocus>
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Barcode</th>
                                <th>Equipment</th>
                                <th>Serial No.</th>
                            </tr>
                        </thead>
                        <tbody id="tableAdd">
                        </tbody>
                    </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
                </form>
        </div>
    </div>
</div>

<script>
    $('input[name=invoice_number]').on('change',function(){
        $('a').attr('href','?no='+$(this).val())
    })

    function pickCart() {
        $('.pick-product').on('click',function(index, el) {
            if($(this).data('package') != '') {
                $('[data-package='+$(this).data('package')+']').prop('checked',$(this).is(':checked'))
            }
        });
    }
    pickCart()

    function findProduct() {
        var typingTimer;                //timer identifier
        var doneTypingInterval = 200;  //time in ms, 5 second for example
        var $input = $('input[name=barcode_search]');
        //on keyup, start the countdown
        $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        //on keydown, clear the countdown 
        $input.on('keydown', function () {
        clearTimeout(typingTimer);
        });

        //user is "finished typing," do something
        function doneTyping () {
            var barcode = $input.val()
            if(barcode == '') {
                return false
            }
            var url = "{{ url('items/find?res_type=json') }}"+"&barcode="+barcode
            $.get(url, function( data ) {
                if(data.length==0) {
                    alert('Item no found')
                }
                else{
                    var item  = data[0]
                    var same = 0
                    $.each($('.item_ids'), function() {
                         if($(this).val() == item.id) {
                            alert('Item has been scanned')
                            same++
                            return false
                         }
                    });

                    var cartId = 0
                    $('.pick-product:checkbox:checked').each(function(index, el) {
                        if($(this).data('product') == item.product_id) {
                            cartId = $(this).data('cart')
                            return
                        }
                    });

                    if(cartId == 0) {
                        alert('Item not match')
                    }

                    if(same == 0 && cartId != 0) {
                        var html = '<tr class="invalid-blink">'+
                        '<td>'+
                        '<input type="hidden" name="cart_id[]" value="'+cartId+'">'+
                        '<input type="hidden" name="product_id[]" value="'+item.product_id+'">'+
                        '<input type="hidden" name="product_name[]" value="'+item.product_name+'">'+
                        '<input type="hidden" name="item_id[]" class="item_ids" value="'+item.id+'">'+
                        '<input type="hidden" name="item_barcode[]" value="'+item.barcode+'">'+
                        '<input type="hidden" name="item_serial_number[]" value="'+item.serial_number+'">'+
                        item.barcode+
                        '</td>'+
                        '<td>'+
                        item.product_name+
                        '</td>'+
                        '<td>'+
                        item.serial_number+
                        '</td>'+
                        '</tr>'
                        $('#tableAdd').append(html)
                        return;
                    }
                }
            });
            $input.val('')
        }
    }
    findProduct()
</script>
@endsection
