@extends('layouts.loginLayout')
@section('content')
<div class="login-wrapper">
    <div class="bg-pic">
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
            <h1 class="semi-bold text-white">
                The whole of life is just like watching a film.
            </h1>
            <p class="small">
                Only it's as though you always get in ten minutes after the big picture has started, and no-one will tell you the plot, so you have to work it out all yourself from the clues.
            </p>
            <p class="small">
                ― Terry Pratchett, Moving Pictures
            </p>
            <p> &nbsp;</p>
        </div>
    </div>
    <div class="login-container bg-white">
        <div class="p-l-50 p-r-50 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <img src="{{asset ('img/logo-48x48_c.png')}}" alt="logo" data-src="{{ asset('img/logo-48x48_c.png' )}}" data-src-retina="{{ asset('img/logo-48x48_c@2x.png')}}" width="48" height="48">
            <h2 class="p-t-25">Get Started <br /> with your Dashboard</h2>
            <p class="mw-80 m-t-5">Sign in to your account</p>
            <form id="form-login" method="POST" action="{{ route('login') }}" class="p-t-15" role="form">
                @csrf
                <div class="form-group form-group-default">
                    <label class="" for="email">Login</label>
                    <div class="controls">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group form-group-default">
                    <label class="" for="password">Password</label>
                    <div class="controls">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Credential">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 no-padding sm-p-l-10">
                    </div>
                    <div class="col-md-6 d-flex align-items-center justify-content-end">
                        <button type="submit" class="btn btn-primary btn-lg m-t-10">
                            Sign In
                        </button>
                        <!-- <a href="index.php" class="btn btn-primary btn-lg m-t-10">Sign in</a> -->
                    </div>
                </div>
            </form>
            <div class="pull-bottom sm-pull-bottom">
                <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
                    <div class="col-sm-12 no-padding m-t-10">
                        <p class="small-text normal hint-text">
                            ©2020 All Rights Reserved. Digital Optik Teknologi.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
