@extends('layouts.adminLayout')

@section('title')
Package List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Packages</a></li>
    <li class="breadcrumb-item active">Packages List</li>
@endsection

@section('content')
<div class="row">
    {{-- <div class="pull-right">
        <a href="{{'packages/create'}}" class="btn btn-info btn-sm text-light">Add</a>
    </div> --}}
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th class="" scope="col">Name</th>
                <th class="" scope="col">Price per Day</th>
                <th class="" scope="col">Price 7 Days</th>
                <th class="" scope="col">Active</th>
                <th class="" scope="col">Available</th>
                <th class="text-center" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($packages as $package)
            <tr style="backgroundColor:#fff">
                <td class="">{{$package->name}}</td>
                <td class="">Rp. {{number_format($package->price_day)}}</td>
                <td class="">Rp. {{number_format($package->price_7days)}}</td>
                <td class="">{{$package->is_active == 1 ? "Yes" : "No"}}</td>
                <td class="">{{$package->is_avail == 1 ? "Yes" : "No"}}</td>
                <td class=" text-center justify-content-center"> 
                    <a href="{{'packages/'.$package->id}}" class="btn btn-info btn-sm text-light">View</a>
                    <a href="{{'packages/'.$package->id.'/edit'}}" class="btn btn-success btn-sm text-light">Edit</a>
                    <form action="{{url('packages/'.$package->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <div class="display-3 text-center">No packages Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection