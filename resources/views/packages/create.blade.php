@extends('layouts.adminLayout')

@section('title')
New Package
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/packages">Packages</a></li>
    <li class="breadcrumb-item active">New Package</li>
@endsection

@section('content')
<div class="row">
<div class=" col-md-12">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add New Packages</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="/packages" class="row" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="col-md-6">
                        <div class="card-title">Detail Pacakge</div>
                        <div class="form-group form-group-default required">
                            <label for="full_name">Name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ old('name') }}" required>
                        </div>
                        <div class="form-group form-group-default required">
                            <label for="full_name">Price per Day</label>
                            <input type="number" name="price_day" min=1000 class="form-control @error('price_day') is-invalid @enderror" id="price_day" value="{{ old('price_day') }}" required>
                        </div>
                        <div class="form-group form-group-default required">
                            <label for="full_name">Price 7 Days</label>
                            <input type="number" name="price_7days" min=1000 class="form-control @error('price_7days') is-invalid @enderror" id="price_7day" value="{{ old('price_7day') }}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-title">Product Include</div>
                        <select class="form-control" id="selectProduct">
                            <option value="0" selected disabled>Select Product</option>
                            @foreach($products as $product)
                            <option id="optionProd{{$product->id}}" value="{{$product->id}}">{{$product->name}}</option>                            
                            @endforeach
                        </select>
                        <a href="#" class="btn btn-default" id="btnPick">Pick</a>
                        <table class="table" id="tblPickProduct">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#btnPick').on('click',function(){
        var productName = $('#selectProduct option:selected').text()
        var productId = $('#selectProduct option:selected').val()
        
        if(productId !=0){
            var html = '<tr id="rowProd'+productId+'">'
            html +='<td><input name="products[]" type="text" value="'+productId+'" class="hidden">'+productName+'</td>'
            html +='<td><input name="qty[]" type="number" min="1" value="1" style="width:30px !important"></td>'
            html +='<td><a class="btn btn-danger text-light btnRemoveProduct" data-id="'+productId+'">Remove</a><td>'
            html +='</tr>'
            $('#tblPickProduct > tbody:last-child').append(html)
            $('#optionProd'+productId).attr('disabled','disabled');
        }
    })
    $('tbody').on('click','.btnRemoveProduct',function(){
        var rowId = $(this).data('id') 
        console.log(rowId)
        $('#rowProd'+rowId).remove()
        $('#optionProd'+rowId).removeAttr('disabled');
    })
</script>
@endsection
