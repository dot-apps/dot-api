<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <title>Invoice</title>
        <style>
            ul {
                padding-inline-start : 20px !important;
            }
        </style>
    </head>
	<body style="font-family: 'Poppins', sans-serif; margin-top: 2cm; margin-bottom: 0.75cm; font-size: 11px;">
		<header style="position: fixed; top: -1.2cm; left: -1.25cm; right: -1.25cm; height: 1.14cm; width: 100%">
			<img src="{{ asset('/img/letterhead/header.svg') }}" width="100%">
			<img src="{{ asset('/img/letterhead/logo.svg') }}" style="width: 90px; margin-left: 1.25cm">
		</header>
		<footer style="position: fixed; bottom: -1.425cm; left: -1.25cm; right: -1.25cm; height: 1.75cm;">
			<p class="text-footer" style="color: #2851a4; color: #2851a4; margin-top: 0; text-align: center; margin-bottom: 5px; font-size: 12px;">Jl. Jeruk Raya &middot; Ruko Soho Jagakarsa No.9B Jakarta Selatan, 12620 &middot; 0857 1168 7748 &middot; hello@dot-rental.com &middot; @dot_rent</p>
			<img src="{{ asset('/img/letterhead/footer.svg') }}" width="100%">
		</footer>
		<main>
			<p style="text-align: center; margin-top: -.75cm; margin-bottom: 0"><b>INVOICE</b></p>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<tbody>
					<tr>
						<td style="padding: 5px; text-align: right; background: #EEEEEE; background-color: transparent; padding-bottom: 0;" bgcolor="transparent" align="right">No :</td>
						<td style="padding: 5px; background: #EEEEEE; width: 20%; background-color: transparent; text-align: left; padding-bottom: 0;" width="20%" bgcolor="transparent" align="left"><b>{{ $transaction->number }}</b></td>
					</tr>
					<tr>
						<td style="padding: 5px; text-align: right; background: #DDDDDD; background-color: transparent; padding-top: 0; padding-bottom: 0;" bgcolor="transparent" align="right">Date :</td>
						<td style="padding: 5px; background: #DDDDDD; width: 20%; background-color: transparent; text-align: left; padding-top: 0; padding-bottom: 0;" width="20%" bgcolor="transparent" align="left"><b>{{ date('d/m/Y',strtotime($transaction->created_at)) }}</b></td>
					</tr>
				</tbody>
			</table>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<tbody>
                    @php
                        $customer = json_decode($transaction['customers'],TRUE);
                    @endphp
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #EEEEEE; text-align: left; width: 20%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Nama</td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 30%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">: <b>{{$customer['customer_name']}}</b></td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 20%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Production</td>
                        <td style="padding: 5px; background: #EEEEEE; text-align: left; width: 30%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>{{$customer['customer_production']}}</b>
                        </td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Company</td>
                        <td style="padding: 5px; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>-</b>
                        </td>
						<td style="padding: 5px; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Location</td>
                        <td style="padding: 5px; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>{{$customer['customer_location']}}</b>
                        </td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #EEEEEE; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Mobile Phone</td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">: <b>{{$customer['customer_phone']}}</b></td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Equipment call</td>
                        <td style="padding: 5px; background: #EEEEEE; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>{{ $customer['customer_equipment_call'] }}</b>
                        </td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">E-Mail</td>
						<td style="padding: 5px; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">: <b>{{ $customer['customer_email'] }}</b></td>
						<td style="padding: 5px; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Date of pick up</td>
                        <td style="padding: 5px; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>{{ date("d/m/Y", strtotime($transaction->start_date)) }}</b>
                        </td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #EEEEEE; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">DOP</td>
                        <td style="padding: 5px; background: #EEEEEE; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>{{ $customer['customer_dop'] }}<b>
                        </td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Return date</td>
                        <td style="padding: 5px; background: #EEEEEE; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>{{ date("d/m/Y", strtotime($transaction->end_date)) }}</b>
                        </td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #DDDDDD; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Gaffer</td>
                        <td style="padding: 5px; background: #DDDDDD; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            : <b>{{ $customer['customer_gaffer'] }}</b>
                        </td>
						<td style="padding: 5px; background: #DDDDDD; text-align: left; width: 20%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="20%" align="left" bgcolor="transparent">Total Days</td>
                        <td style="padding: 5px; background: #DDDDDD; text-align: left; width: 30%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="30%" align="left" bgcolor="transparent">
                            @php
                                $diff = date_diff(date_create($transaction->start_date),date_create($transaction->end_date));
                            @endphp
                            : <b>{{ $diff->format('%a Days') }}</b>
                        </td>
					</tr>
				</tbody>
			</table>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<thead>
					<tr>
						<th class="desc" style="padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal; text-align: left;" align="left">DESKRIPSI</th>
						<th class="" style="text-align: center; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">UNIT</th>
						<th style="text-align: center; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">HARI</th>
						<th style="text-align: center; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">HARGA SATUAN</th>
						<th style="text-align: center; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="center">HARGA PERHARI</th>
					</tr>
				</thead>
				<tbody>
                    @php $i=1 @endphp
                    @foreach ($carts as $key=>$_cart)
                        <tr>
                            <td class="desc" style="padding: 5px; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }}text-align: left; vertical-align: top;" align="left" valign="top">
                                {!!$_cart->product_name!!}
                            </td>
                            <td class="qty" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                {!!$_cart->qty !!}
                            </td>
                            <td class="qty" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                {!!$_cart->day !!}
                            </td>
                            <td class="qty" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                Rp{{ number_format($_cart->price,null,null,'.') }}
                            </td>
                            <td class="total" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                Rp{{ number_format($_cart->total,null,null,'.') }}
                            </td>
                        </tr>
                        @php $i++ @endphp
                    @endforeach
					<tr>
						<td colspan="4" class="sub" style="padding: 5px; text-align: right; border-top: 1px solid #C1CED9;" align="right">JUMLAH</td>
                        <td class="sub total" style="padding: 5px; text-align: right; vertical-align: top; border-top: 1px solid #C1CED9;" align="right" valign="top">
                            RP{{ number_format($transaction->cart_subtotal,0,',','.')   }}
                        </td>
                    </tr>
                    @php
                        $discounts = json_decode($transaction->cart_discounts,TRUE);
                        $i=1;
                    @endphp
                    @foreach ($discounts[0] as $_discount)
                        @if ($_discount['type'] == '7d' && $_discount['amount'] != 0)
                        <tr>
    						<td colspan="4" style="padding: 5px; text-align: right; background: #EEEEEE;" align="right">DISKON (> 7 Hari)</td>
                            <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                                Rp{{number_format(str_replace('-','',$_discount['amount']),null,null,'.')}}
                            </td>
                        </tr>
                        @elseif($_discount['type'] == 'add' && $_discount['amount'] != 0)
                        <tr>
                            <td colspan="4" style="padding: 5px; text-align: right; background: #EEEEEE;" align="right">
                                DISKON ({{ $_discount['percentage'] }} %)
                            </td>
                            <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                                Rp{{number_format(str_replace('-','',$_discount['amount']),null,null,'.')}}
                            </td>
                        </tr>
                        @endif
                        @php $i++; @endphp
                    @endforeach

                    @php
                        $paymentsList = json_decode($payments->list,TRUE);
                        $i=1;
                    @endphp
                    @if (sizeof($guards) == 0)
                        @if (sizeof($paymentsList) !== 0)
                            @foreach ($paymentsList as $_payment)
                            <tr>
                                <td colspan="4" style="padding: 5px; text-align: right; background: #EEEEEE;" align="right">
                                    {{ ($_payment->sequence == 1) ? 'DP' : 'PEMBAYARAN KE - '.$_payment->sequence }}
                                </td>
                                <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                                    Rp{{number_format(str_replace('-','',$_payment->amount),null,null,'.')}}
                                </td>
                            </tr>
                            @endforeach
                        @endif

                        <tr style="border-bottom: 1px solid #C1CED9">
                            <td colspan="4" class="" style="padding: 5px; text-align: right;" align="right"><b>SISA PEMBAYARAN</b></td>
                            <td class="" style="padding: 5px; text-align: right;" align="right">
                                <b>Rp{{number_format(str_replace('-','',$balance->amount),null,null,'.')}}</b>
                            </td>
                        </tr>
                    @else
                    <tr style="border-bottom: 1px solid #C1CED9">
						<td colspan="4" class="" style="padding: 5px; text-align: right;" align="right"><b>SISA PEMBAYARAN</b></td>
                        <td class="" style="padding: 5px; text-align: right;" align="right">
                            <b>Rp{{number_format(str_replace('-','',$transaction->cart_total),null,null,'.')}}</b>
                        </td>
                    </tr>
                    @foreach ($guards as $_guard)
                    <tr>
                        <td class="desc" style="padding: 5px; background: #EEEEEE; text-align: left; vertical-align: top;" align="left" valign="top">
                            {!! $_guard->name !!}
                        </td>
                        <td class="qty" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            {!! $_guard->qty !!}
                        </td>
                        <td class="qty" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            {!! $_guard->day !!}
                        </td>
                        <td class="qty" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            <!-- <span style="display: inline-grid;width: 10%;" >Rp</span>
                            <span style="display: inline-grid;width: 88%;text-align: right;"> -->
                                Rp{{number_format(str_replace('-','',$_guard->price),null,null,'.')}}
                            <!-- </span> -->
                        </td>
                        <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            Rp{{number_format(str_replace('-','',$_guard->total),null,null,'.')}}
                        </td>
					</tr>
                    @endforeach
                    @foreach ($others as $_other)
                    <tr>
                        <td class="desc" style="padding: 5px; background: #EEEEEE; text-align: left; vertical-align: top;" align="left" valign="top">
                            {!! $_other->name !!}
                        </td>
                        <td class="qty" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            {!! $_other->qty !!}
                        </td>
                        <td class="qty" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            {!! $_other->day !!}
                        </td>
                        <td class="qty" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            <!-- <span style="display: inline-grid;width: 10%;" >Rp</span>
                            <span style="display: inline-grid;width: 88%;text-align: right;"> -->
                                Rp{{number_format(str_replace('-','',$_other->price),null,null,'.')}}
                            <!-- </span> -->
                        </td>
                        <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                            Rp{{number_format(str_replace('-','',$_other->total),null,null,'.')}}
                        </td>
                    </tr>
                    @endforeach
					<tr>
						<td colspan="4" class="sub" style="padding: 5px; text-align: right; border-top: 1px solid #C1CED9;" align="right">TOTAL</td>
                        <td class="sub total" style="padding: 5px; text-align: right; vertical-align: top; border-top: 1px solid #C1CED9;" align="right" valign="top">
                            Rp{{number_format(str_replace('-','',$transaction->total),null,null,'.')}}
                        </td>
                    </tr>
                        @if (sizeof($paymentsList) !== 0)
                            @foreach ($paymentsList as $_payment)
                            <tr>
                                <td colspan="4" style="padding: 5px; text-align: right; background: #EEEEEE;" align="right">
                                    {{ ($_payment['sequence'] == 1) ? 'DP' : 'PEMBAYARAN KE - '.$_payment['sequence'] }}
                                </td>
                                <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                                    Rp{{number_format(str_replace('-','',$_payment['amount']),null,null,'.')}}
                                </td>
                            </tr>
                            @endforeach
                        @endif
					<tr style="border-bottom: 1px solid #C1CED9">
						<td colspan="4" class="" style="padding: 5px; text-align: right; background: #DDDDDD;" align="right"><b>SISA PEMBAYARAN</b></td>
                        <td class="" style="padding: 5px; text-align: right; background: #DDDDDD;" align="right">
                            <b>Rp{{number_format(str_replace('-','',$balance->amount),null,null,'.')}}</b>
                        </td>
					</tr>
                    @endif

				</tbody>
			</table>
			<div id="notices">
				<div>Terbilang : <b><em>{{ $balance->string }}</em></b></div>
				<div>Harap Transfer ke Rek Berikut :</div>
				<ul class="notice" style="padding-inline-start: 15px; padding-left: 0;">
					<li style="display: block;"><b>Mandiri : 1270010431722</b></li>
					<li style="display: block;"><b>BCA : 5470658811</b></li>
					<li style="display: block;"><b>Atas nama: PT Digital Optik Teknologi</b></li>
				</ul>
			</div>
			<div id="notices">
				<div>KETERANGAN:</div>
				<ol class="notice" style="padding-inline-start: 15px;">
					<li>Biaya pengantaran alat dengan menggunakan jasa pengantaran menjadi tanggungjawab penyewa</li>
					<li>Keterlambatan pengembalian alat dikenakan biaya 50% dari harga sewa</li>
					<li>Waktu pengembalian alat paling lambat pukul 01.00 WIB</li>
					<li>Waktu pengembalian alat lewat pukul 03.00 WIB dikenakan biaya full</li>
					<li>Waktu OT Pengawal dihitung dari jam 00:00 WIB (00:00-03:00 WIB 50%, lebih jam 03:00 WIB full)</li>
					<li>Pembayaran minimal 50% diawal ,selanjutnya 50% ketika pengembalian alat</li>
					<li>Wajib melakukan pengecekan terlebih dahulu sebelum melakukan peminjaman</li>
					<li>Apabila sudah melakukan pengecekan tetapi saat dilokasi ada alat yang tertinggal atau rusak menjadi tanggung jawab penyewa</li>
				</ol>
			</div>
			<div id="notices">
				<div>Jakarta, {{ $tanggal }}</div>
				<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 30px;" width="100%">
					<tbody>
						<tr>
							<td style="padding: 5px; background: #EEEEEE; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background: #EEEEEE; background-color: transparent; width: 40%; text-align: center; height: .15cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background: #EEEEEE; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">Penyewa</td>
						</tr>
						<tr>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 40%; text-align: center; height: .75cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"></td>
						</tr>
						<tr>
							<td style="padding: 5px; background: #DDDDDD; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"><u>Utami</u><br>Cost & Control</td>
							<td style="padding: 5px; background: #DDDDDD; background-color: transparent; width: 40%; text-align: center; height: .15cm;" width="40%" bgcolor="transparent" align="center"></td>
                            <td style="padding: 5px; background: #DDDDDD; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">
                                {{ $customer['customer_name'] }}
                            </td>
						</tr>
					</tbody>
				</table>
			</div>
		</main>
 	</body>
</html>
