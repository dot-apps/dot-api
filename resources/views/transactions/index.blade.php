@extends('layouts.adminLayout')

@section('title')
Transactions List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Transactions</a></li>
    <li class="breadcrumb-item active">Transaction List</li>
@endsection

@section('content')
<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr >
                <th  scope="col">#</th>
                <th  scope="col">Number</th>
                <th scope="col">Customer</th>
                <th scope="col">Dates</th>
                <th scope="col"  class="text-right">Amount</th>
                <th scope="col"  class="text-right">Paid</th>
                <th scope="col"  class="text-right">Outstanding</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @php $no=1 @endphp
        @forelse ($transactions as $transaction)
            <tr style="backgroundColor:#fff">
                <td>{{ $no }}</td>
                <td>{{ $transaction->number }}</td>
                @php
                    $customer = json_decode($transaction->customers,true);
                    $customer = $customer['customer_name'].' ('.$customer['customer_phone'].')';
                @endphp
                <td>{{$customer}}</td>
                <td>
                    {{ date("M-d", strtotime($transaction->start_date)) }} to {{ date("M-d", strtotime($transaction->end_date)) }}
                </td>
                <td >
                    <span class="pull-left">Rp.</span>
                    <span class="pull-right">{{number_format($transaction->total,null,null,'.')}}</span>

                </td>
                <td>
                    <span class="pull-left">Rp.</span>
                    <span class="pull-right">
                        {{number_format($transaction->paid_amount,null,null,'.')}}
                    </span>
                </td>
                <td>
                    <span class="pull-left">Rp.</span>
                    <span class="pull-right">
                        {{number_format($transaction->total-$transaction->paid_amount,null,null,'.')}}
                    </span>
                </td>
                <td>{{$transaction->status_name}}</td>
                <td class="justify-content-center">
                    <a href="{{'transactions/unduh/'.$transaction->id}}" class="btn btn-info btn-sm text-light">PDF</a>
                    <a href="{{'transactions/'.$transaction->id}}" class="btn btn-info btn-sm text-light">View</a>
                    <!-- <a href="{{'transactions/'.$transaction->id.'/edit'}}" class="btn btn-success btn-sm text-light">Edit</a>
                    <form action="{{url('transactions/'.$transaction->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                    </form> -->
                </td>
            </tr>
            @php $no++ @endphp
        @empty
            <div class="display-3 text-center">No transactions Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection
