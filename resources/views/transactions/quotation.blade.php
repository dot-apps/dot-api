{{-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>{{ $title }}</title>
	</head>
	<body>
		<h1>{{ $heading}}</h1>
		<div>
			<p>{{$content}}</p>
		</div>
	</body>
</html>wh --}}
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Quotation</title>
	</head>
	<body style="font-family: 'Poppins', sans-serif; margin-top: 2cm; margin-bottom: 0.75cm; font-size: 11px;">
		<header style="position: fixed; top: -1.2cm; left: -1.25cm; right: -1.25cm; height: 1.14cm; width: 100%">
			<img src="{{ asset('/img/letterhead/header.svg') }}" width="100%">
			<img src="{{ asset('/img/letterhead/logo.svg') }}" style="width: 90px; margin-left: 1.25cm">
		</header>
		<footer style="position: fixed; bottom: -1.425cm; left: -1.25cm; right: -1.25cm; height: 1.75cm;">
			<p class="text-footer" style="color: #2851a4; color: #2851a4; margin-top: 0; text-align: center; margin-bottom: 5px; font-size: 12px;">Jl. Jeruk Raya &middot; Ruko Soho Jagakarsa No.9B Jakarta Selatan, 12620 &middot; 0857 1168 7748 &middot; hello@dot-rental.com &middot; @dot_rent</p>	
			<img src="{{ asset('/img/letterhead/footer.svg') }}" width="100%">
		</footer>
		<main>
			<p style="text-align: center; margin-top: -.75cm; margin-bottom: 20px"><b>SURAT PENAWARAN</b></p>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<tbody>
					@php
                        $customer = json_decode($transaction['customers'],TRUE);
                    @endphp
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #EEEEEE; text-align: left; width: 10%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="10%" align="left" bgcolor="transparent">Tanggal</td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 90%; background-color: transparent; padding-top: 0; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: {{ $tanggal }}</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #EEEEEE; text-align: left; width: 10%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="10%" align="left" bgcolor="transparent">Dari </td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 90%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: Digital Optik Teknologi</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #EEEEEE; text-align: left; width: 10%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="10%" align="left" bgcolor="transparent">Kepada</td>
						<td style="padding: 5px; background: #EEEEEE; text-align: left; width: 90%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: {{$customer['customer_name']}}</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; background: #DDDDDD; text-align: left; width: 10%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="10%" align="left" bgcolor="transparent">Hal </td>
						<td style="padding: 5px; background: #DDDDDD; text-align: left; width: 90%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: Penawaran Harga</td>
					</tr>
					<!-- <tr>
						<td style="padding: 5px; padding-left: 0; background: #DDDDDD; text-align: left; width: 10%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="10%" align="left" bgcolor="transparent">No </td>
						<td style="padding: 5px; background: #DDDDDD; text-align: left; width: 90%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: {{ $transaction->number }}</td>
					</tr> -->
					<!-- <tr>
						<td style="padding: 5px; padding-left: 0; background: #DDDDDD; text-align: left; width: 10%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="10%" align="left" bgcolor="transparent">Versi </td>
						<td style="padding: 5px; background: #DDDDDD; text-align: left; width: 90%; background-color: transparent; padding-top: 3px; padding-bottom: 0;" width="90%" align="left" bgcolor="transparent">: {{ $transaction->version }}</td>
					</tr> -->
				</tbody>
			</table>
			<div id="notices" style="margin-bottom: 10px;">
				<div>Dengan Hormat,</div>
				<div>Memenuhi permintaan Bapak/Ibu,mengenai harga sewa di Digital Optik Teknologi (DOT). Berikut dilampirkan spesifikasi peralatan :</div>
			</div>
			<table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<thead>
					<tr>
						<th class="desc" style="padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal; text-align: left;" align="left">DESKRIPSI</th>
						<th class="" style="text-align: right; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="right">UNIT</th>
						<th style="text-align: right; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="right">HARI</th>
						<th style="text-align: right; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="right">HARGA SATUAN</th>
						<th style="text-align: right; padding: 5px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;" align="right">HARGA PER HARI</th>
					</tr>
				</thead>
				<tbody>
					@php $i=1 @endphp
                    @foreach ($carts as $key=>$_cart)
                        <tr>
                            <td class="desc" style="padding: 5px; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }}text-align: left; vertical-align: top;" align="left" valign="top">
                                {!!$_cart->product_name!!}
                            </td>
                            <td class="qty" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                {!!$_cart->qty !!}
                            </td>
                            <td class="qty" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                {!!$_cart->day !!}
                            </td>
                            <td class="qty" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                Rp{{ number_format($_cart->price,null,null,'.') }}
                            </td>
                            <td class="total" style="padding: 5px; text-align: right; {{ ($i%2==0) ?  'background: #EEEEEE;' : '' }} vertical-align: top;" align="right" valign="top">
                                Rp{{ number_format($_cart->total,null,null,'.') }}
                            </td>
                        </tr>
                        @php $i++ @endphp
                    @endforeach

                    <tr>
						<td colspan="4" class="sub" style="padding: 5px; text-align: right; border-top: 1px solid #C1CED9;" align="right">JUMLAH</td>
                        <td class="sub total" style="padding: 5px; text-align: right; vertical-align: top; border-top: 1px solid #C1CED9;" align="right" valign="top">
                            Rp{{ number_format($transaction->cart_subtotal,0,',','.')   }}
                        </td>
                    </tr>
                    @php
                        $discounts = json_decode($transaction->cart_discounts,TRUE);
                        $i=1;
                    @endphp
                    @foreach ($discounts[0] as $_discount)
                        @if ($_discount['type'] == '7d' && $_discount['amount'] != 0)
                        <tr>
    						<td colspan="4" style="padding: 5px; text-align: right; background: #EEEEEE;" align="right">DISKON BARANG (> 7 Hari)</td>
                            <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                                Rp{{number_format(str_replace('-','',$_discount['amount']),null,null,'.')}}
                            </td>
                        </tr>
                        @elseif($_discount['type'] == 'add' && $_discount['amount'] != 0)
                        <tr>
                            <td colspan="4" style="padding: 5px; text-align: right; background: #EEEEEE;" align="right">
                                DISKON ({{ $_discount['percentage'] }} %)
                            </td>
                            <td class="total" style="padding: 5px; text-align: right; background: #EEEEEE; vertical-align: top;" align="right" valign="top">
                                Rp{{number_format(str_replace('-','',$_discount['amount']),null,null,'.')}}
                            </td>
                        </tr>
                        @endif
                        @php $i++; @endphp
                    @endforeach

					<tr style="border-bottom: 1px solid #C1CED9">
						<td colspan="4" class="" style="padding: 5px; text-align: right; background: #DDDDDD;" align="right"><b>TOTAL</b></td>
						<td class="" style="padding: 5px; text-align: right; background: #DDDDDD;" align="right">
							<b>Rp{{ number_format($transaction->total,0,',','.')   }}</b>
					</tr>


				</tbody>
			</table>
			<div id="notices" style="margin-bottom: 10px">
				@php
					$dp = $transaction->total/100*50;
				@endphp
				<div>Pemakaian alat selama {{ $transaction->days }} hari pada tanggal {{ $tanggal_start }} sampai {{ $tanggal_end }}</div>
				<div>Pembayaran DP minimal 50% sebesar Rp{{number_format($dp)}} (<spans style="text-transform: capitalize;">{{ $balance->dp }}</span>)</div>
				<div>Pembayaran pelunasan maksimal waktu pengembalian alat</div>
			</div>
			<div id="notices">
				<div>KETERANGAN:</div>
				<ol class="notice" style="padding-inline-start: 15px;">
					<li>Biaya pengantaran alat dengan menggunakan jasa pengantaran menjadi tanggungjawab penyewa </li>
					<li>Keterlambatan pengembalian alat dikenakan biaya 50% dari harga sewa</li>
					<li>Waktu pengembalian alat paling lambat pukul 01.00 WIB</li>
					<li>Waktu pengembalian alat lewat pukul 03.00 WIB dikenakan biaya full</li>
					<li>Waktu OT Pengawal dihitung dari jam 00:00 WIB (00:00-03:00 WIB 50%, lebih jam 03:00 WIB full)</li>
					<li>Pembayaran minimal 50% diawal ,selanjutnya 50% ketika pengembalian alat</li>
					<li>Wajib melakukan pengecekan terlebih dahulu sebelum melakukan peminjaman</li>
					<li>Apabila sudah melakukan pengecekan tetapi saat dilokasi ada alat yang tertinggal atau rusak menjadi tanggung jawab penyewa</li>
				</ol>
			</div>
			<div id="notices">
				<div>Harap Transfer ke Rek Berikut :</div>
				<ul class="notice" style="padding-inline-start: 15px; padding-left: 0;">
					<li style="display: block;"><b>Mandiri : 1270010431722</b></li>
					<li style="display: block;"><b>BCA : 5470658811</b></li>
					<li style="display: block;"><b>Atas nama: PT Digital Optik Teknologi</b></li>
				</ul>
			</div>
			<div id="notices">
				
				<table style=" border-collapse: collapse; border-spacing: 0; margin-bottom: 30px;">
					<tbody>
						<tr>
							<td style="padding: 5px; background: #EEEEEE; background-color: transparent; text-align: center;" bgcolor="transparent" align="center">
								Jakarta, {{ $tanggal }}
							</td>
						</tr>
						<tr>
							<td style="padding: 5px; background-color: transparent; text-align: center;"  bgcolor="transparent" align="center"></td>
							<!-- <td style="padding: 5px; background-color: transparent; width: 40%; text-align: center; height: .75cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center"></td> -->
						</tr>
						<tr>
							<td style="padding: 5px; background: #DDDDDD; background-color: transparent; text-align: center;"  bgcolor="transparent" align="center">Utami</td>
							<!-- <td style="padding: 5px; background: #DDDDDD; background-color: transparent; width: 40%; text-align: center; height: .15cm;" width="40%" bgcolor="transparent" align="center"></td>
							<td style="padding: 5px; background: #DDDDDD; background-color: transparent; width: 30%; text-align: center;" width="30%" bgcolor="transparent" align="center">{{$customer['customer_name']}}</td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</main>
 	</body>
</html>