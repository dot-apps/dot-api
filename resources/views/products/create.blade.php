@extends('layouts.adminLayout')

@section('title')
New Product
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/products">Products</a></li>
    <li class="breadcrumb-item active">New Product</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add new product</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="/products" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group form-group-default required">
                        <label for="full_name">Category</label>
                        <input type="text" name="category" class="form-control" list="categories" />
                        <datalist id="categories">
                        @forelse ($categories as $category)
                            <option>{{ $category->category}}</option>
                        @empty
                        @endforelse
                        </datalist>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Brand</label>
                        <input type="text" name="brand" class="form-control" list="brands" />
                        <datalist id="brands">
                        @forelse ($brands as $brand)
                            <option>{{ $brand->brand}}</option>
                        @empty
                        @endforelse
                        </datalist>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ old('name') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="desc">Description</label>
                        <textarea name="desc" id="desc" class="form-control" cols="30" rows="10">{{ old('desc')}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="full_name">Price per Day</label>
                        <input type="number" name="price_day" min=1000 class="form-control @error('price_day') is-invalid @enderror" id="price_day" value="{{ old('price_day') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="full_name">Price 7 Days</label>
                        <input type="number" name="price_7days" min=1000 class="form-control @error('price_7days') is-invalid @enderror" id="price_7day" value="{{ old('price_7day') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="photo">Upload Photo</label>
                        <input type="file" class="form-control @error('photo') is-invalid @enderror" id="photo" value="{{ old('photo') }}" name="photo" required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection