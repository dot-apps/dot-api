@extends('layouts.adminLayout')

@section('title')
Detail Product - {{$product->name}}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/products">Products</a></li>
    <li class="breadcrumb-item active">Product Detail</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title"></div>
            </div>
            <div class="card-body">
                <h3 class="text-center">{{$product->name}}</h3>
                <div class="text-center mb-4">
                    <img class="img-thumbnail text-center" src="{{($product->photo == NULL) ? '/img/no-photo.png' : '/storage/' . $product->photo }}" alt="image">
                </div>
                <p><strong>Name: </strong> {{$product->name}}</p> 
                <p><strong>Desc: </strong> {{$product->desc}}</p> 
                <p><strong>Status: </strong> {{$product->is_active == 1 ? 'ACTIVE' : 'UN-ACTIVE' }}</p> 
            </div>
        </div>
    </div>
    <!-- <div class="col-md-2"></div> -->
    <div class=" col-md-8">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Items</div>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active" data-toggle="tab" role="tab" data-target="#tab2hellowWorld" href="#" aria-selected="true">In House</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab" data-target="#tab2FollowUs" class="" aria-selected="false">Partner</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2hellowWorld">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Barcode</th>
                                            <th>Serial Number</th>
                                            <th>Description</th>
                                            <th>Purchase At</th>
                                            <th>Added At</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no= 1?>
                                    @forelse ($items as $_item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $_item->barcode }}</td>
                                            <td>{{ $_item->serial_number }}</td>
                                            <td>{{ $_item->desc }}</td>
                                            <td>{{ $_item->purchase_at }}</td>
                                            <td>{{ $_item->created_at }}</td>
                                            <td class="text-center">
                                                @if ($_item->status == 1)
                                                    <button type="button" class="btn btn-success">Avail</button>
                                                @elseif($_item->status == 2)
                                                    <button type="button" class="btn btn-primary">On Rent</button>
                                                @else
                                                    <button type="button" class="btn btn-danger">Broke</button>
                                                @endif
                                            </td>
                                        </tr>
                                        <?php $no++?>
                                    @empty
                                        <p>No Items</p>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2FollowUs">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Barcode</th>
                                            <th>Serial Number</th>
                                            <th>Description</th>
                                            <!-- <th>Purchase At</th> -->
                                            <th>Added At</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no= 1?>
                                    @forelse ($partner_items as $_item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $_item->barcode }}</td>
                                            <td>{{ $_item->serial_number }}</td>
                                            <td>{{ $_item->desc }}</td>
                                            <td>{{ $_item->purchase_at }}</td>
                                            <td>{{ $_item->created_at }}</td>
                                            <td class="text-center">
                                                @if ($_item->status == 1)
                                                    <button type="button" class="btn btn-success">Avail</button>
                                                @elseif($_item->status == 2)
                                                    <button type="button" class="btn btn-primary">On Rent</button>
                                                @else
                                                    <button type="button" class="btn btn-danger">Broke</button>
                                                @endif
                                            </td>
                                        </tr>
                                        <?php $no++?>
                                    @empty
                                        <tr>
                                            <td colspan="8" class="text-center">No Items</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal" id="modelAddNewItem" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generate New Serial Number</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="newItemForm" method="POST" action="/items">
                    @csrf
                    <input type="product_id" class="hidden" name="product_id" value="{{$product->id}}">
                    <div class="form-group row">
                        <label for="purchase_at" class="col-sm-1-12 col-form-label">
                            Purchase At
                        </label>
                        <input type="date" class="form-control" name="purchase_at" id="purchase_at" placeholder="" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" form="newItemForm">Save</button>
            </div>                
        </div>
    </div>
</div>

@endsection