@extends('layouts.adminLayout')

@section('title')
Product List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Products</a></li>
    <li class="breadcrumb-item active">Products List</li>
@endsection

@section('content')
<div class="row">
    {{-- <a href="{{'products/create'}}" class="btn btn-info btn-sm text-light">Add</a> --}}
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Category</th>
                <th scope="col">Brand</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Price per Day</th>
                <th scope="col">Price 7 Days</th>
                <th scope="col">Active</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($products as $product)
            <tr style="backgroundColor:#fff">
                <td>{{$product->category}}</td>
                <td>{{$product->brand}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->desc}}</td>
                <td>Rp. {{number_format($product->price_day)}}</td>
                <td>Rp. {{number_format($product->price_7days)}}</td>
                <td>{{$product->is_active == 1 ? "Yes" : "No"}}</td>
                <td class="justify-content-center"> 
                    <a href="{{'products/'.$product->id}}" class="btn btn-info btn-sm text-light">View</a>
                    <a href="{{'products/'.$product->id.'/edit'}}" class="btn btn-success btn-sm text-light">Edit</a>
                    <form action="{{url('products/'.$product->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <div class="display-3 text-center">No Products Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection