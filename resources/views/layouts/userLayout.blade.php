<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <title>Dashboard</title>

    <link rel="apple-touch-icon" href="{{ asset('assets/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('ico/76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('ico/favicon.ico') }}" />
    <link href="{{ asset('plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link class="main-stylesheet" href="{{ asset('css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    @yield('head')
</head>
<body>
    {{-- <body class="fixed-header menu-pin"> --}}
    <body class="fixed-header">
        @guest
        @else
        <nav class="page-sidebar" data-pages="sidebar">
            <div class="sidebar-header">
                <img src="{{ asset('img/logo_white.png')}}" alt="logo" class="brand" data-src="{{ asset('img/logo_white.png')}}" data-src-retina="{{ asset('img/logo_white_2x.png')}}" width="78" height="22">
            </div>
            <div class="sidebar-menu">
                <ul class="menu-items">
                    <li class="active">
                        <a href="javascript:void(0);" id="mnDashboard" class="">
                            <span class="title">Dashboard</span>
                        </a>
                        <span class="icon-thumbnail"><i class="pg-icon">home</i></span>
                    </li>
                    @include('layouts.userSidebar')
                </ul>
                <div class="clearfix"></div>
            </div>
        </nav>
        <div class=" page-container ">
            @include('layouts.userNavbar')
            <div class=" page-content-wrapper ">
                <div class="content">
                    <div class="jumbotron" data-pages="parallax">
                        <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
                            <div class="inner">
                                <ol class="breadcrumb">
                                    @yield('breadcrumb')
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid container-fixed-lg">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        @endguest
    </div>
    <script src="{{ asset('plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/liga.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/popper/umd/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-actual/jquery.actual.min.js')}}"></script>
    <script src="{{ asset('plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/classie/classie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages.js')}}"></script>
    <script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/meus.js') }}" type="text/javascript"></script>
    @yield('script')
</body>
</html>
