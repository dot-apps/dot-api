<li class="{{ Request::segment(1) === 'users' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Users</span>
        <span class="arrow"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">users</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'users' && Request::segment(2) === null || Request::segment(2) === 'edit' ? 'active' : null }}">
            <a  href="{{ url('users' )}}">User List</a>
            <span class="icon-thumbnail"><i class="pg-icon">ul</i></span>
        </li>
        {{-- <li class="">
            <a href="/roles">Roles</a>
            <span class="icon-thumbnail"><i class="pg-icon">rs</i></span>
        </li> --}}
        <li class="{{ Request::segment(1) === 'users' && Request::segment(2) === 'create' ? 'active' : null }}">
            <a href="{{ url('/users/create' )}}">New User</a>
            <span class="icon-thumbnail"><i class="pg-icon">nu</i></span>
        </li>
    </ul>
</li>