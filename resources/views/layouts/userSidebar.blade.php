<li class="{{ Request::segment(1) === 'products' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Products</span>
        <span class="arrow {{ Request::segment(1) === 'products' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">list_bullets</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'products' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/products' )}}">Product List</a>
            <span class="icon-thumbnail"><i class="pg-icon">pl</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'products' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/products/create'}}">New Product</a>
            <span class="icon-thumbnail"><i class="pg-icon">np</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'packages' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Packages</span>
        <span class="arrow {{ Request::segment(1) === 'packages' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">folder</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'packages' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/packages' )}}">Package List</a>
            <span class="icon-thumbnail"><i class="pg-icon">pl</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'packages' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/packages/create'}}">New Package</a>
            <span class="icon-thumbnail"><i class="pg-icon">np</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'operators' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Operators</span>
        <span class="arrow {{ Request::segment(1) === 'operators' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'operators' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/operators' )}}">Operator List</a>
            <span class="icon-thumbnail"><i class="pg-icon">ol</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'operators' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/operators/create'}}">New Operator</a>
            <span class="icon-thumbnail"><i class="pg-icon">no</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'customers' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Customers</span>
        <span class="arrow {{ Request::segment(1) === 'customers' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'customers' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/customers' )}}">Customers List</a>
            <span class="icon-thumbnail"><i class="pg-icon">cl</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'customers' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/customers/create'}}">New Customer</a>
            <span class="icon-thumbnail"><i class="pg-icon">nc</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'partners' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Partners</span>
        <span class="arrow {{ Request::segment(1) === 'partners' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'partners' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/partners' )}}">Partner List</a>
            <span class="icon-thumbnail"><i class="pg-icon">pl</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'partners' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/partners/create'}}">New Partner</a>
            <span class="icon-thumbnail"><i class="pg-icon">np</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'inventories' || Request::segment(1) === 'inventories/track' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Inventories</span>
        <span class="arrow {{ Request::segment(1) === 'invetories' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">camera</i></span>
    <ul class="sub-menu">
        <li class="{{ (Request::segment(1) === 'inventories' && Request::segment(2) === '') ? 'active' : null }}">
            <a  href="{{ url('inventories' )}}">List Inventory</a>
            <span class="icon-thumbnail"><i class="pg-icon">LI</i></span>
        </li>
        <li class="{{ Request::segment(2) === 'partner' ? 'active' : null }}">
            <a  href="{{ url('inventories/partner' )}}">Partners</a>
            <span class="icon-thumbnail"><i class="pg-icon">IO</i></span>
        </li>
        <li class="{{ Request::segment(2) === 'inout' ? 'active' : null }}">
            <a  href="{{ url('inventories/inout' )}}">In Out</a>
            <span class="icon-thumbnail"><i class="pg-icon">IO</i></span>
        </li>
        {{-- <li class="{{ Request::segment(2) === 'track' ? 'active' : null }}">
            <a  href="{{ url('inventories/track' )}}">Track Inventory</a>
            <span class="icon-thumbnail"><i class="pg-icon">ti</i></span>
        </li> --}}
    </ul>
</li>

{{-- <li class="{{ Request::segment(1) === 'quotations' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Quotations</span>
        <span class="arrow {{ Request::segment(1) === 'quotations' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'quotations' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/quotations' )}}">Quotations List</a>
            <span class="icon-thumbnail"><i class="pg-icon">ol</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'quotations' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/quotations/create'}}">New Quotation</a>
            <span class="icon-thumbnail"><i class="pg-icon">no</i></span>
        </li>
    </ul>
</li> --}}

<li class="{{ Request::segment(1) === 'transactions' ? ' open active' : null }}">
    <a href="javascript:void(0);">
        <span class="title">Transactions</span>
        <span class="arrow {{ Request::segment(1) === 'transactions' ? ' open active' : null }}"></span>
    </a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'transactions' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/transactions/create'}}">New Transaction</a>
            <span class="icon-thumbnail"><i class="pg-icon">nt</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'transactions' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/transactions' )}}">Transactions List</a>
            <span class="icon-thumbnail"><i class="pg-icon">tl</i></span>
        </li>
    </ul>
</li>
