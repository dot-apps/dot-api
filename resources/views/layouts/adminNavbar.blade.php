<div class="header ">
    <a href="#" class="btn-link toggle-sidebar d-lg-none pg-icon btn-icon-link" data-toggle="sidebar">
        menu</a>
    <div class="">
        <div class="brand inline   ">
            <img src="{{ asset('/img/logo.png') }}" alt="logo" data-src="{{ asset('/img/logo.png') }}" data-src-retina="{{ asset('/img/logo_2x.png') }}" width="78" height="22">
        </div>
        <ul class="d-lg-inline-block d-none notification-list no-margin d-lg-inline-block b-grey b-l b-r no-style p-l-20 p-r-20">
            <li class="p-r-5 inline">
                <b>PT Digital Optik Teknologi</b>
            </li>
        </ul>
    </div>
    <div class="d-flex align-items-center">
        <div class="dropdown pull-right d-lg-block d-none">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="profile dropdown">
                <span class="thumbnail-wrapper d32 circular inline">
                    <img src="{{ asset('/img/profiles/avatar.jpg') }}" alt="" data-src="{{ asset('/img/profiles/avatar.jpg') }}" data-src-retina="{{ asset('/img/profiles/avatar_small2x.jpg') }}" width="32" height="32">
                </span>
            </button>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                <a href="#" class="dropdown-item"><span>Signed in as <br /><b>{{ Auth::user()->name }}</b></span></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                </a>
            </div>
        </div>
    </div>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
