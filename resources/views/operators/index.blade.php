@extends('layouts.adminLayout')

@section('title')
Operator List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Operators</a></li>
    <li class="breadcrumb-item active">Opertators List</li>
@endsection

@section('content')
<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th class="" scope="col">Name</th>
                <th class="" scope="col">Phone</th>
                <th class="" scope="col">Email</th>
                <th class="" scope="col">Fee</th>
                <th class="" scope="col">Active</th>
                <th class="" scope="col">Available</th>
                <th class="text-center" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($operators as $operator)
            <tr style="backgroundColor:#fff">
                <td class="">{{$operator->name}}</td>
                <td class="">{{$operator->phone}}</td>
                <td class="">{{$operator->email}}</td>
                <td class="">Rp. {{number_format($operator->fee)}}</td>
                <td class="">{{$operator->is_active == 1 ? "Yes" : "No"}}</td>
                <td class="">{{$operator->is_avail == 1 ? "Yes" : "No"}}</td>
                <td class=" text-center justify-content-center"> 
                    <a href="{{'operators/'.$operator->id}}" class="btn btn-info btn-sm text-light">View</a>
                    <a href="{{'operators/'.$operator->id.'/edit'}}" class="btn btn-success btn-sm text-light">Edit</a>
                    <form action="{{url('operators/'.$operator->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <div class="display-3 text-center">No Operators Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection