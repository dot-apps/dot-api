@extends('layouts.adminLayout')

@section('title')
Quotations List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Quotations</a></li>
    <li class="breadcrumb-item active">Quotation List</li>
@endsection

@section('content')
<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Customer</th>
                <th scope="col">Start Date</th>
                <th scope="col">Expired Date</th>
                <th scope="col">Amount</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        @php $no=1 @endphp
        @forelse ($quotations as $quotation)
            <tr style="backgroundColor:#fff">
                <td>{{$no}}</td>
                @php
                    $customer = json_decode($quotation->customers,true);
                    $customer = $customer['customer_name'].' ('.$customer['customer_phone'].')';
                @endphp
                <td>{{$customer}}</td>
                <td>{{$quotation->date}}</td>
                <td>{{$quotation->expired_at}}</td>
                <td>Rp.{{number_format($quotation->subtotal)}}</td>
                <td class="justify-content-center"> 
                    <a href="{{'quotations/unduh/'.$quotation->id}}" class="btn btn-info btn-sm text-light">PDF</a>
                    <a href="{{'quotations/'.$quotation->id}}" class="btn btn-info btn-sm text-light">View</a>
                    <!-- <a href="{{'quotations/'.$quotation->id.'/edit'}}" class="btn btn-success btn-sm text-light">Edit</a>
                    <form action="{{url('quotations/'.$quotation->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                    </form> -->
                </td>
            </tr>
            @php $no++ @endphp
        @empty
            <div class="display-3 text-center">No quotations Available</div>
        @endforelse
        </tbody>
    </table>
</div>
@endsection