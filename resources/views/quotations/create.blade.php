@extends('layouts.adminLayout')

@section('title')
New Product
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/quotations">Quotations</a></li>
    <li class="breadcrumb-item active">New Quotation</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-12">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add new quotations</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="/quotations" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                    
                        <div id="card-linear-color" class="card card-primary col-md-6">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">
                                    Customer Data
                                </div>
                                <div class="card-title pull-right font-weight-bold">
                                    <a class="btn btn-info btn-sm text-light" id="btnCustFind" data-toggle="modal" data-target="#modalCust">FIND</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:20%">Name</th>
                                            <th>:</th>
                                            <th style="width:80%">
                                                <input value="" class="form-control" name="customer_id" id="inputCustId" hidden/>
                                                <input value="" class="form-control" name="customer_name" id="inputCustName" readonly/>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">Phone</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                <input value="" class="form-control" name="customer_phone" id="inputCustPhone" readonly/>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">Email</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                <input value="" class="form-control" name="customer_email" id="inputCustEmail" readonly/>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">Address</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                <input value="" class="form-control" name="customer_address" id="inputCustAddress" readonly/>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        
                        <div id="card-linear-color" class="card card-primary col-md-6">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">Dates</div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:25%">Quotation Date</th>
                                            <th>:</th>
                                            <th style="width:75%">
                                                <input type="date" value="" class="form-control" name="quotation_date"/>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">Expired Date</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                <input type="date" value="" class="form-control" name="quotation_expired"/>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div id="card-linear-color" class="card card-primary col-md-12">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">CART</div>
                                <div class="card-title pull-right font-weight-bold">
                                    <a class="btn btn-info btn-sm text-light" id="btnCartModal" data-toggle="modal" data-target="#modalCart">+ADD</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tblCart">
                                    <thead>
                                        <tr>
                                            <th style="width:50%">Deksripsi</th>
                                            <th style="width:8%" class="text-center">Unit</th>
                                            <th style="width:8%" class="text-center">Hari</th>
                                            <th style="width:17%" class="text-center">Harga Satuan</th>
                                            <th style="width:17%" class="text-center">Total</th>
                                            <th style="width:17%" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>

                                <table class="table table-bordered" id="tblCartSum">
                                    <tbody>
                                        <!-- <tr>
                                            <td class="text-right font-weight-bold" style="width:76%">
                                                DISCOUNT FOR < 7 DAYS USES
                                            </td>
                                            <td class="total_discount" style="width: 16%">
                                                <input type="number" value="1500000" min="1" class="form-control text-center total_amount" name="product_total[]" readonly/>
                                            </td>
                                            <td style="width: 17%"></td>
                                        </tr> -->
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:76%">
                                                JUMLAH
                                            </td>
                                            <td class="" style="width: 16%">
                                                <input type="number" value="1500000" min="1" class="form-control text-center total_amount" name="grand_total" readonly/>
                                            </td>
                                            <td style="width: 17%"></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- CUSTOMER MODAL -->
<div class="modal fade" id="modalCust" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Customer Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for=""></label>
                    <input type="text" placeholder="please input customer phone" class="form-control" id="mdlCustInputFind"/>
                </div>
                <div class="col-md-12 text-center">
                    <a class="btn btn-primary " id="btnCustFind">FIND</a>    
                </div>
                <br>
                <div class="col-md-12 text-center" id="divCustFindResult"></div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-primary" id="btnCustChoose">Choose</button>
            </div>
        </div>
    </div>
</div>
<!-- CUSTOMER -->

<div class="modal right fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Price List</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="searchTablePriceList" class="form-control pull-right" placeholder="Search">
                <br>
                <br>
                <table class="table table-hover" id="tablePriceList">
                    <thead>
                        <tr>
                            <th class="col-md-12 font-weight-bold">Product</th>
                            <th class="col-md-3 text-right font-weight-bold">1 Day</th>
                            <th class="col-md-3 text-right font-weight-bold">7 Days</th>
                            <th class="text-center font-weight-bold">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $id=0 ?>
                        @foreach($list as $_list)

                        <tr>
                            <td id="plDesc{{$id}}">
                                {{$_list->name}}<br>
                                @if($_list->type == 2)
                                    <ul>
                                    <?php $i = json_decode($_list->package_product,TRUE) ?>
                                    @foreach($i as $_i)
                                        <li>{{$_i['category']}} {{$_i['brand']}} {{$_i['name']}} x {{$_i['qty']}}</li>
                                    @endforeach
                                    </ul>
                                @endif
                            </td>
                            <td id="plPriceDay{{$id}}" class="text-right" data-int="{{$_list->price_day}}">
                                Rp.{{number_format($_list->price_day)}}
                            </td>
                            <td id="plPrice7Days{{$id}}" class="text-right" data-int="{{$_list->price_7days}}">
                                Rp.{{number_format($_list->price_7days)}}
                            </td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-pick-pl" data-id="{{$id}}" data-prod-uid="{{$_list->uid}}" data-prod-type="{{$_list->type}}">Pick</a>
                            </td>
                        </tr>
                        <?php $id++ ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>


<style>
input[readonly] {
    background-color: #e5ebf9 !important;
    color:black !important; 
}
</style>
<script>
    var customers = <?=json_encode($customers)?>;
    $('#modalCust').on('click','#btnCustFind',function(){
        var custToFind = $('#mdlCustInputFind').val();
        var custResults = []
        for(var key in customers) {
            var custLoop = customers[key]
            
            if(custLoop.phone === custToFind ) {
                var html = '<table class="table">'+
                '        <tbody>'+
                '            <tr>'+
                '                <td style="width:5%" class="text-left">Type</td>'+
                '                <td class="text-center">:</td>'+
                '                <td style="width:95%" class="text-left">'+((custLoop.type == 1) ? 'Personal' : 'Corporate')+'</td>'+
                '            </tr>'+
                '            <tr>'+
                '                <td style="width:5%" class="text-left">Name</td>'+
                '                <td class="text-center">:</td>'+
                '                <td style="width:95%" class="text-left">'+custLoop.name+'</td>'+
                '            </tr>'+
                '            <tr>'+
                '                <td style="width:5%" class="text-left">Phone</td>'+
                '                <td class="text-center">:</td>'+
                '                <td style="width:95%" class="text-left">'+custLoop.phone+'</td>'+
                '            </tr>'+
                '            <tr>'+
                '                <td style="width:5%" class="text-left">Email</td>'+
                '                <td class="text-center">:</td>'+
                '                <td style="width:95%" class="text-left">'+custLoop.email+'</td>'+
                '            </tr>'+
                '            <tr>'+
                '                <td style="width:5%" class="text-left">Address</td>'+
                '                <td class="text-center">:</td>'+
                '                <td style="width:95%" class="text-left">'+custLoop.address+'</td>'+
                '            </tr>'+
                '        </tbody>'+
                '    </table>';

                $('#divCustFindResult').html(html)
                $('#btnCustChoose').data('custData', custLoop)
                break;
            }
            else {
                var html = '<p class="text-danger text-center">CUSTOMER DATA NOT FOUND</p>'
                html +='<a class="btn btn-info text-center text-light" id="btnAddNewCust">Add New Customer</a>'
                $('#divCustFindResult').html(html)
            }
        }
    })
    $('#modalCust').on('click','#btnCustChoose',function(){
        const custData = $(this).data('custData')
        $('#inputCustId').val(custData.id)
        $('#inputCustName').val(custData.name)
        $('#inputCustPhone').val(custData.phone)
        $('#inputCustEmail').val(custData.email)
        $('#inputCustAddress').val(custData.address)
        $('#modalCust').modal('hide')
    })
</script>
<script>

    function calcCost() {
        var total = 0;
        var discounts = 0;
        $('#tblCart tr').each( function() {
            if ( $(this).find('td').length && $(this).find('td input').length ) {
                var quant = parseInt($(this).find('td input').eq(2).val())
                var days = parseInt($(this).find('td input').eq(3).val())
                var price = parseInt($(this).find('td input').eq(4).val());
                var amount = quant * days * price
                $(this).find('.amount').val(amount);

                if(days >= 7) {
                    // var pD7 =  parseInt($(this).find('td input').eq(2).data('7days'))
                    // var sD = days-7
                    // var pD = price*sD
                    // var tPDis = pD7+pD
                    // var discount = tPDis-amount
                    // discounts=discount+discounts
                    // amount = amount+discounts
                }
                
                // console.log('price= '+price)
                // console.log('qty= '+quant)
                // console.log('days= '+days)
                // console.log('total= '+quant*price*days)

                total = total+amount
            }
            // console.log(total) 
        });
        $('.total_amount').val(total);
        // $('.total_discount').html(discounts);
    }

    calcCost();

    $('#tblCart').on('change','input', function() {
        console.log('a')
        calcCost();
    });

    var initTablePriceList = function() {
        var tablePriceList = $('#tablePriceList');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            // "oLanguage": {
            //     "sLengthMenu": "_MENU_ ",
            //     "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            // },
            "iDisplayLength": 10
        };

        tablePriceList.dataTable(settings);

        // search box for table
        $('#searchTablePriceList').keyup(function() {
            tablePriceList.fnFilter($(this).val());
        });

        $('.btn-pick-pl').click(function(event) {
            var rowId = $(this).data('id')
            var html = '<tr>'+
            '<td>'+$('#plDesc'+rowId).html()+'</td>'+
            '<td>'+
            '<textarea name="product_name[]" class="hidden" readonly>'+$('#plDesc'+rowId).html()+'</textarea>'+
            '<input type="text" value="'+$(this).data('prod-uid')+'"  class="form-control text-center hidden" name="product_uid[]" readonly/>'+
            '<input type="text" value="'+$(this).data('prod-type')+'"  class="form-control text-center hidden" name="product_type[]" readonly/>'+
            '<input type="number" value="1" min="1" class="form-control text-center" name="product_qty[]"/>'+
            '</td>'+
            '<td><input type="number" value="1" min="1" class="form-control text-center" name="product_day[]"/></td>'+
            '<td><input type="number" data-day="'+$('#plPriceDay'+rowId).data('int')+'" data-7days="'+$('#plPrice7Days'+rowId).data('int')+'" value="'+$('#plPriceDay'+rowId).data('int')+'" min="1" class="form-control text-center" name="product_price[]" readonly/></td>'+
            '<td><input type="number" value="'+$('#plPriceDay'+rowId).data('int')+'" min="1" class="form-control text-center amount" name="product_total[]" readonly/></td>'+
            '<td><a href="#" class="btn btn-danger">REMOVE</a></td>'+
            '</tr>'

            $('#tblCart').find('tr:last').after(html)
            calcCost();
        });
    }
    initTablePriceList()
</script>
@endsection