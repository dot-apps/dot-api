<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();


Route::get('/', 'LandingController@index');
Route::get('/home', 'HomeController@index')->middleware(['web','auth']);
Route::get('/admin-panel', 'AdminController@index')->middleware(['web','auth']);

Route::get('/users', 'UsersController@index')->middleware(['web','auth']);
Route::get('/users/create', 'UsersController@create')->middleware(['web','auth']);
Route::post('/users/store', 'UsersController@store')->middleware(['web','auth']);
Route::get('/users/edit/{id}', 'UsersController@edit')->middleware(['web','auth']);
Route::get('/users/{id}', 'UsersController@show')->middleware(['web','auth']);
Route::put('/users/{id}', 'UsersController@update')->middleware(['web','auth']);
Route::delete('/users/{id}', 'UsersController@destroy')->middleware(['web','auth']);

Route::get('/roles', 'RolesController@index')->middleware(['web','auth']);
Route::get('/roles/{id}', 'RolesController@show')->middleware(['web','auth']);

Route::get('/profile', 'ProfileController@show')->middleware(['web','auth']);
Route::get('/profile/edit', 'ProfileController@edit')->middleware(['web','auth']);
Route::put('/profile', 'ProfileController@update')->middleware(['web','auth']);

// MASTER DATA
Route::resource('operators', 'OperatorsController')->middleware(['web','auth']);
Route::resource('products', 'ProductsController')->middleware(['web','auth']);

Route::get('items/find', 'ItemsController@find')->middleware(['web','auth']);
Route::resource('items', 'ItemsController')->middleware(['web','auth']);

Route::resource('packages', 'PackagesController')->middleware(['web','auth']);
Route::resource('customers', 'CustomersController')->middleware(['web','auth']);

Route::resource('partners', 'PartnersController')->middleware(['web','auth']);

// QUOTATIONS
Route::get('quotations/unduh/{id}', 'QuotationsController@unduh')->middleware(['web','auth']);
Route::resource('quotations', 'QuotationsController')->middleware(['web','auth']);


// TRANSACTIONS
Route::get('transactions/dokumen/{id}/{action}', 'TransactionsController@dokumen')->middleware(['web','auth']);
Route::resource('transactions', 'TransactionsController')->middleware(['web','auth']);



// Inventory module
Route::get('inventories/inout', 'InventoriesController@inout')->middleware(['web','auth']);
Route::get('inventories/inout/add', 'InventoriesController@inout_add')->middleware(['web','auth']);
Route::get('inventories/inout/{id}', 'InventoriesController@inout_show')->middleware(['web','auth']);
Route::post('inventories/inout', 'InventoriesController@inout_store')->middleware(['web','auth']);
Route::get('inventories/inout/unduh/{trans_id}/{inout_id}', 'InventoriesController@inout_unduh')->middleware(['web','auth']);

Route::get('inventories/partner', 'InventoriesController@partner')->middleware(['web','auth']);

Route::post('inventories/partner', 'InventoriesController@partner_store')->middleware(['web','auth']);
Route::get('inventories/partner/{id}', 'InventoriesController@partner_show')->middleware(['web','auth']);
Route::get('inventories/partner/in/{id}', 'InventoriesController@partner_in')->middleware(['web','auth']);
Route::get('inventories/partner/out/{id}', 'InventoriesController@partner_out')->middleware(['web','auth']);
Route::get('inventories/partner/unduh/{id}', 'InventoriesController@partner_unduh')->middleware(['web','auth']);
Route::delete('inventories/partner/{id}/{trans_id}', 'InventoriesController@partner_destroy')->middleware(['web','auth']);
Route::resource('inventories', 'InventoriesController')->middleware(['web','auth']);
// #Inventory module
