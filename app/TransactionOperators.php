<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionOperators extends Model
{
    protected $table = "transaction_operators";
	protected $primaryKey = "id";
}
