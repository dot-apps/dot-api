<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageProducts extends Model
{
    protected $table = "package_products";
	protected $primaryKey = "id";
}
