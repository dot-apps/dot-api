<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use App\Products;
use App\Items;
use App\Packages;
use App\PackageProducts;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = DB::table('packages')
                        ->orderBy('packages.created_at', 'ASC')
                        ->paginate(20);
        return view('packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Products::all();
        return view('packages.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'price_day' => 'required',
            'price_7days' => 'required',
        ]);
        
        $package = new Packages;
        $package->name = $request->name;
        $package->price_day = $request->price_day;
        $package->price_7days = $request->price_7days;
        $package->save();
        $package_id = $package->id;

        $products = $request->products;
        $qtys = $request->qty;
        for ($i=0; $i < sizeof($products) ; $i++) { 
            $packageProduct = new PackageProducts;
            $packageProduct->package_id = $package_id;
            $packageProduct->product_id = $products[$i];
            $packageProduct->qty = $qtys[$i];
            $packageProduct->save();
        }

        
        return redirect('packages')->with('msg_success', 'Package Created Successfully');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Packages::find($id);
        $packageProducts = PackageProducts::where('package_id','=',$id)
        ->join('products','products.id','=','package_products.product_id')
        ->get();
        $products = Products::all();
        return view('packages.view', compact('package','packageProducts','products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Packages::find($id);
        $packageProducts = PackageProducts::where('package_id','=',$id)
        ->join('products','products.id','=','package_products.product_id')
        ->get();
        $products = Products::all();
        return view('packages.edit', compact('package','packageProducts','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'price_day' => 'required',
            'price_7days' => 'required',
        ]);

        $package = Packages::find($id);
        $package->name = $request->name;
        $package->price_day = $request->price_day;
        $package->price_7days = $request->price_7days;
        $package->save();
        
        $packageProducts = PackageProducts::where('package_id','=',$id);
        $packageProducts->delete();

        $products = $request->products;
        $qtys = $request->qty;
        for ($i=0; $i < sizeof($products) ; $i++) { 
            $packageProduct = new PackageProducts;
            $packageProduct->package_id = $id;
            $packageProduct->product_id = $products[$i];
            $packageProduct->qty = $qtys[$i];
            $packageProduct->save();
        }

        return redirect('packages')->with('msg_success', 'Package Edited Successfully');
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $packageProduct = PackageProducts::find($id);
        $packageProduct->delete();

        $package = Packages::find($id);
        $package->delete();

        return redirect('packages')->with('msg_success', 'Package Deleted Successfully');
    }
}
