<?php

namespace App\Http\Controllers;
use App\Products;
use App\Items;
use App\Customers;
use App\CustomerPics;
use App\Operators;
use App\Quotations;
use DB;
use PDF;

use Illuminate\Http\Request;

class QuotationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotations = DB::table('quotations')
                        ->orderBy('quotations.created_at', 'DESC')
                        ->paginate(20);
        return view('quotations.index', compact('quotations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $products = Products::select('id','name as text')->get();
        $operators = Operators::All();
        $customers = Customers::All();
        $list = DB::select("SELECT 
                        1 AS type,
                        prod.id AS uid,
                        prod.name AS name,
                        prod.price_day AS price_day,
                        prod.price_7days AS price_7days,
                        '' AS package_product
                    FROM
                        products prod
                    WHERE
                        prod.is_active = 1
                    UNION
                    SELECT 
                        2 AS type,
                        pkg.id AS uid,
                        pkg.name AS name,
                        pkg.price_day AS price_day,
                        pkg.price_7days AS price_7days,
                        CONCAT('[',
                            GROUP_CONCAT(
                                JSON_OBJECT(
                                    'id',p.id,
                                    'category',p.category,
                                    'brand',p.brand,
                                    'name',p.name,
                                    'qty',pp.qty
                                )
                            ),
                            ']') AS package_product
                    FROM
                        packages pkg
                            INNER JOIN
                        package_products pp ON pkg.id = pp.package_id
                            INNER JOIN
                        products p ON p.id = pp.product_id
                    WHERE
                        pkg.is_active = 1
                    GROUP BY pkg.id");
                
        return view('quotations.create', compact('operators','customers','list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customerData = Customers::find($request->customer_id);

        $customer = (object) [
            'customer_id' => $request->customer_id,
            'customer_name' => $request->customer_name,
            'customer_phone' => $request->customer_phone,
            'customer_email' => $customerData['email'],
            'customer_address' => $customerData['address'],
        ];

        $product_types = $request->product_type;
        $product_uids = $request->product_uid;
        $product_names = $request->product_name;
        $product_qtys = $request->product_qty;
        $product_days = $request->product_day;
        $product_prices = $request->product_price;
        $product_totals = $request->product_total;

        $products = [];
        foreach ($request->product_type as $key=>$val) {
            $product = new \stdClass();
            $product->types = $product_types[$key];
            $product->uids = $product_uids[$key];
            $product->names = $product_names[$key];
            $product->qtys = $product_qtys[$key];
            $product->days = $product_days[$key];
            $product->prices = $product_prices[$key];
            $product->totals = $product_totals[$key];
            array_push($products, $product);
        }
        

        $quotation = new Quotations;
        $quotation->customer_id = $request->customer_id;
        $quotation->date = $request->quotation_date;
        $quotation->customers = json_encode($customer);
        $quotation->carts = json_encode($products);
        $quotation->subtotal = $request->grand_total;
        $quotation->expired_at = $request->quotation_expired;
        $quotation->save();
        
        return redirect('quotations')->with('msg_success', 'Quotation Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quotation = Quotations::find($id);
        return view('quotations.view', compact('quotation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unduh($id)
    {
        
        $quotation = Quotations::find($id);
        $customer = json_decode($quotation['customers'],TRUE);
        $custName = str_replace(' ', '', $customer['customer_name']);
        $custName = substr($custName, 0, 5);
        $namePdf = 'QUO-'.$custName.'.pdf';
        $pdf = PDF::loadView('quotations.unduh', compact('quotation'));
        return $pdf->download($namePdf);

        // $pdf = PDF::loadView('quotations.invoice', $data);
        // return $pdf->download('invoice.pdf');

        // return view('quotations.unduh', compact('quotation'));
    }
}
