<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class MasterData extends Controller
{
    public function productList()
    {
        return view('product/list', compact('users_count'));
    }
    public function categoryList()
    {
        return view('product/category', compact('users_count'));
    }
}
