<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use App\Products;
use App\Items;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')
                        ->orderBy('products.created_at', 'DESC')
                        ->paginate(20);
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DB::table('products')
                ->select('category')
                ->groupBy('category')
                ->get();

        $brands = DB::table('products')
                ->select('brand')
                ->groupBy('brand')
                ->get();

        return view('products.create', compact('categories','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validatedData = $request->validate([
            'category' => 'required',
            'brand' => 'required',
            'name' => 'required|unique:products',
            'desc' => 'required',
            'photo' => 'required',
            'price_day' => 'required',
            'price_7days' => 'required',
        ]);

        $product = new Products;
        $product->category = $request->category;
        $product->brand = $request->brand;
        $product->name = $request->name;
        $product->desc = $request->desc;
        $product->price_day = $request->price_day;
        $product->price_7days = $request->price_7days;

        if($request->hasFile('photo')) {
            $product->photo = $request->photo->store('products', 'public');
        }

        $product->save();

        return redirect('products')->with('msg_success', 'Products Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::find($id);
        $items = DB::table('items')
                    ->where('product_id','=',$id)
                    ->where('type','=',1)
                    ->get();

        $partner_items = DB::table('items')
                    ->where('product_id','=',$id)
                    ->where('type','=',2)
                    ->get();
        return view('products.view', compact('product','items','partner_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = DB::table('products')
                ->select('category')
                ->groupBy('category')
                ->get();

        $brands = DB::table('products')
                ->select('brand')
                ->groupBy('brand')
                ->get();
        $product = Products::findOrFail($id);
        return view('products.edit', compact('product','categories','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataValidate = [
            'category' => 'required',
            'brand' => 'required',
            'desc' => 'required',
            'photo' => 'required',
            'price_day' => 'required',
            'price_7days' => 'required',
        ];


        $product = Products::find($id);
        if($product->name != $request->name) {
            $dataValidate['name'] = 'required|unique:products';
        }

        $validatedData = $request->validate($dataValidate);

        $product->category = $request->category;
        $product->brand = $request->brand;
        $product->name = $request->name;
        $product->desc = $request->desc;
        $product->price_day = $request->price_day;
        $product->price_7days = $request->price_7days;

        if($request->hasFile('photo')) {
            $product->photo = $request->photo->store('products', 'public');
        }

        $product->save();
        return redirect('products')->with('msg_success', 'Products Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('items')->where('product_id', $id)->delete();
        $product = Products::find($id);
        $product->delete();
        return redirect('products')->with('msg_success', 'Product Deleted Successfully');
    }
}
