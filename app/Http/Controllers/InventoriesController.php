<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Products;
use App\Packages;
use App\PackageProducts;
use App\Items;
use App\Customers;
use App\CustomerPics;
use App\Operators;
use App\Guards;
use App\Quotations;
use App\Transactions;
use App\TransactionPayments;
use App\TransactionCarts;
use App\Inout;
use App\InoutItems;
use App\Partners;
use App\PartnerTransactions;
use App\PartnerProducts;
use App\PartnerItems;
use DB;
use PDF;
use Hashids\Hashids;

class InventoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = DB::table('products')
                    ->leftjoin(
                        DB::raw('(
                            SELECT
                                product_id,
                                COUNT(id) AS jml,
                                SUM(IF(status=1,1,0)) as avail,
                                SUM(IF(status=2,1,0)) as on_rent,
                                SUM(IF(status=3,1,0)) as broken
                            FROM
                                items
                            GROUP BY product_id
                            ) items'),
                            'items.product_id','=','products.id')
                    ->get();

        // $items = DB::table('items')
        //             ->leftjoin('products','products.id','=','items.product_id')
        //             ->orderBy('items.created_at', 'DESC')
        //             ->paginate(20);

        return view('inventories.index',compact('list'));
    }

    public function inout(Request $request)
    {
        $list = Transactions::
                    select(
                        'transactions.id',
                        'transactions.number',
                        'jml_item',
                    )
                    ->leftJoin(
                        DB::raw(
                            '(SELECT 
                                transaction_id,
                                COUNT(id) as jml_item
                            FROM 
                                inout_items
                            GROUP BY
                                transaction_id) inout_items'),'inout_items.transaction_id','=','transactions.id')
                    ->orderBy('transactions.created_at','DESC')
                    ->get();

        return view('inventories.inout',compact('list'));
    }

    public function inout_show($trans_id)
    {
        $transaction = DB::table('transactions')
                        ->select(
                            'transactions.*',
                            'ts.name as status_name',
                            DB::raw('sum(tp.amount) as paid_amount'),
                            DB::raw('(sum(tp.amount)-total) as outstanding_amount')
                        )
                        ->leftJoin('transaction_statuses as ts','ts.code','=','transactions.status')
                        ->leftJoin('transaction_payments as tp','tp.transaction_id','=','transactions.id')
                        ->where('transactions.id','=',$trans_id)
                        ->get()
                        ->first();

        $carts = TransactionCarts::where('transaction_id','=',$trans_id)
        ->orderBy('cart_id','ASC')
        ->get();

        
        $inout = Inout::select(
                        'inout.*',
                        DB::raw("CONCAT('[',
                                    GROUP_CONCAT(
                                        JSON_OBJECT(
                                            'product_name',
                                            product_name,
                                            'barcode',
                                            item_barcode,
                                            'serial_no',
                                            item_serial_number
                                        )
                                    ),
                                ']') as items"))
                    ->leftJoin( 'inout_items','inout_items.inout_id','=','inout.id')
                    ->where('inout.transaction_id','=',$trans_id)
                    ->groupby('inout.id')
                    ->orderBy('inout.created_at','DESC')
                    ->orderBy('inout_items.product_id','ASC')
                    ->get();

        return view('inventories.inout_view',compact('transaction','carts','inout'));
    }

    public function inout_add(Request $request) 
    {
        $data = [];
        $no='';
        if($request->no) {
            $no=$request->no;
            $transaction = Transactions::where('number','=',$request->no)->first();
            if($transaction) {

                $cart = \json_decode($transaction->cart,TRUE);
                $products = [];
                $packages = [];

                foreach($cart as $_cart) {

                    $type = $_cart['types'];
                    if($type == 1) {
                        $product = new \stdClass();
                        $product->cart_id = $_cart['cart_id'];
                        $product->id = $_cart['uids'];
                        $product->name = $_cart['names'];
                        $product->qty = $_cart['qtys'];
                        $product->day = $_cart['days'];
                        array_push($products,$product);
                    }
                    elseif($type == 2) {
                        $packageProducts = Packages::select(
                                                    'products.*',
                                                    'packages.name as package_name',
                                                    'package_products.qty as product_qty')
                                                ->leftJoin('package_products','package_products.package_id','=','packages.id')
                                                ->leftJoin('products','products.id','=','package_products.product_id')
                                                ->where('packages.id','=',$_cart['uids'])
                                                ->get();

                        foreach($packageProducts as $_pp) {
                            $package = new \stdClass();
                            $package->cart_id = $_cart['cart_id'];
                            $package->id = $_cart['uids'];
                            $package->name = $_pp->package_name;
                            $package->product_id = $_pp->id;
                            $package->product_name = $_pp->name;
                            $package->product_qty = $_pp->product_qty;
                            $package->qty = $_cart['qtys'];
                            $package->day = $_cart['days'];
                            array_push($packages,$package);
                        }
                    }
                }

                $data['transaction'] = $transaction;
                $data['products'] = $products;
                $data['packages'] = $packages;
            }
        }

        $data['no'] = $no;
        return view('inventories.inout_add',$data);
    }

    public function inout_store(Request $request) 
    {
        $type = $request->type;
        $transaction_id = $request->transaction_id;

        $nextCount = Inout::select(DB::raw('count(id) as curr'))->where('transaction_id','=',$transaction_id)->first();
        $nextCount = $nextCount->curr+1;

        $hashids = new Hashids('', 6,'abcdefghijklmnopqrstuvwxyz123456789');
        $number = $hashids->encode($transaction_id.$type.$nextCount);
        
        $given_by = $request->given_by;
        $receiver_by = $request->receiver_by;

        $cart_ids = $request->cart_id;
        $item_ids = $request->item_id;
        $product_ids = $request->product_id;
        $product_names = $request->product_name;
        $item_barcodes = $request->item_barcode;
        $item_serial_numbers = $request->item_serial_number;

        DB::beginTransaction();
        $inout = new Inout;
        $inout->type = $type;
        $inout->number = strtoupper($number);
        $inout->transaction_id = $transaction_id;
        $inout->given_by = $given_by;
        $inout->receiver_by = $receiver_by;
        $inout->save();

        $inout_id = $inout->id;

        $items = [];

        foreach ($item_ids as $key=>$_val) {

            $arr = array(
                'inout_id' => $inout_id,
                'transaction_id' => $transaction_id,
                'cart_id' => $cart_ids[$key],
                'item_id' => $item_ids[$key],
                'product_id' => $product_ids[$key],
                'product_name' => $product_names[$key],
                'item_barcode' => $item_barcodes[$key],
                'item_serial_number' => $item_serial_numbers[$key]
            );

            array_push($items, $arr);
        }

        InoutItems::insert($items);

        DB::commit();

        return redirect('inventories/inout/'.$transaction_id)->with('msg_success', 'Transactions Created Successfully');


        
    }

    public function inout_unduh($trans_id,$inout_id)
    {

        $transaction = Transactions::find($trans_id);

        $customer = json_decode($transaction->customers,true);

        $tanggal = self::tanggal_indo(date('Y-m-d',strtotime($transaction->created_at)));

        $inout = Inout::find($inout_id);
        $inout_items = InoutItems::select(DB::raw(
                                'count(product_id) as jml'),
                                'product_name',
                                DB::raw("group_concat(item_serial_number) as serial_no"))
                        ->where('inout_id','=',$inout_id)
                        ->groupBy('product_id')
                        ->orderBy('product_id','ASC')
                        ->get();
        if($inout->type == 1) {
            $namePdf = 'SURAT KELUAR'.'.pdf';
        }
        else {
            $namePdf = 'SURAT KEMBALI'.'.pdf';
        }
        
        $pdf = PDF::loadView('inventories.inout_unduh', compact('transaction','customer','tanggal','inout','inout_items'));
        return $pdf->download($namePdf);

        return view('inventories.inout_unduh', compact('transaction','customer','tanggal','inout','inout_items'));
    }

    public function partner(Request $request)
    {
        $list = Partners::select('partners.*',DB::raw('count(partner_items.id) as qty'))
            ->leftJoin('partner_items','partner_items.partner_id','=','partners.id')
            ->groupBy('partners.id')->get();

        return view('inventories.partner',compact('list'));
    }

    public function partner_show($id)
    {
        $partner = Partners::find($id);
        $transactions = DB::select("
                SELECT 
                    partran.*,
                    CONCAT('[',
                            GROUP_CONCAT(JSON_OBJECT('product_id',
                                        product_id,
                                        'product_name',
                                        product_name,
                                        'qty',
                                        parpro.qty)),
                            ']') AS products
                FROM
                    partner_transactions partran
                        LEFT JOIN
                    partner_products parpro ON parpro.partner_transaction_id = partran.id
                WHERE
                    partran.partner_id = ".$id."
                GROUP BY partran.id
                ORDER BY partran.created_at DESC
            ");
        return view('inventories.partner_view',compact('partner','transactions'));
    }

    public function partner_unduh($id)
    {
        $transactions = DB::select("
                SELECT 
                    partran.*,
                    CONCAT('[',
                            GROUP_CONCAT(JSON_OBJECT('product_id',
                                        product_id,
                                        'product_name',
                                        product_name,
                                        'qty',
                                        parpro.qty)),
                            ']') AS products
                FROM
                    partner_transactions partran
                        LEFT JOIN
                    partner_products parpro ON parpro.partner_transaction_id = partran.id
                WHERE
                    partran.id = ".$id."
                GROUP BY partran.id
                ORDER BY partran.created_at DESC
            ");
        $transaction = $transactions[0];
        $partner = Partners::find($transaction->partner_id);

        $transaction->tanggal = self::tanggal_indo(date('Y-m-d',strtotime($transaction->created_at)));
        $transaction->jam = date('H:m',strtotime($transaction->created_at));
        if($transaction->type == 1) {
            $namePdf = 'SURAT MASUK '.$transaction->code.'.pdf';
        }
        else {
            $namePdf = 'SURAT KEMBALI '.$transaction->code.'.pdf';
        }
        
        $pdf = PDF::loadView('inventories.partner_unduh', compact('partner','transaction'));
        return $pdf->download($namePdf);

        // return view('inventories.partner_unduh', compact('partner','transaction'));
    }

    public function partner_in($id)
    {
        $partners = Partners::find($id);
        $products = Products::all();

        return view('inventories.partner_in',compact('partners','products'));
    }

    public function partner_out($id)
    {
        $partners = Partners::find($id);
        $products = Products::all();

        return view('inventories.partner_out',compact('partners','products'));
    }

    public function partner_store(Request $request)
    {
        if($request->type == 1) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'partner_transactions'");
            $nextId = $statement[0]->Auto_increment;
            $hashids = new Hashids('', 6,'abcdefghijklmnopqrstuvwxyz123456789');
            $code = $hashids->encode($nextId);

            $partner_id = $request->partner_id;
            $type = $request->type;
            $giver = $request->giver;
            $receiver = $request->receiver;
            $note = $request->note;

            $product_ids = $request->product_id;
            $product_names = $request->product_name;
            $product_qtys = $request->product_qty;
            $product_prices = $request->product_price;
            $product_items = $request->product_item;

            DB::beginTransaction();

            $pt = new PartnerTransactions;
            $pt->partner_id = $partner_id;
            $pt->type = $type;
            $pt->code = $code;
            $pt->giver = $giver;
            $pt->receiver = $receiver;
            $pt->note = $note;
            $pt->save();
            $pt_id = $pt->id;

            $products = [];

            foreach ($product_ids as $key=>$val) {
                $pp = new PartnerProducts;
                $pp->partner_id = $partner_id;
                $pp->partner_transaction_id = $pt_id;
                $pp->product_id = $product_ids[$key];
                $pp->product_name = $product_names[$key];
                $pp->qty = $product_qtys[$key];
                $pp->price = $product_prices[$key];
                $pp->save();
                $pp_id = $pp->id;

                foreach ($product_items[$key] as $keyItem=>$value) {

                    $items = [];
                    $statement = DB::select("SHOW TABLE STATUS LIKE 'items'");
                    $nextId = $statement[0]->Auto_increment;
                    
                    $hashids = new Hashids('', 6,'abcdefghijklmnopqrstuvwxyz123456789');
                    $barcode = $hashids->encode($nextId);

                    $ps = new PartnerItems;
                    $ps->partner_id = $partner_id;
                    $ps->partner_transaction_id = $pt_id;
                    $ps->partner_product_id = $pp_id;
                    $ps->serial_number = strtoupper($product_items[$key][$keyItem]);
                    $ps->barcode = strtoupper($barcode);
                    $ps->save();

                    $item = new Items;
                    $item->product_id = $product_ids[$key];
                    $item->type = 2;
                    $item->barcode = strtoupper($barcode);
                    $item->serial_number = strtoupper($product_items[$key][$keyItem]);
                    $item->purchase_at = date('Y-m-d');
                    $item->save();
                    
                }
            }
            DB::commit();
            return redirect('inventories/partner/'.$partner_id)->with('msg_success', 'Transactions Created Successfully');
        }
        else if($request->type == 2) {

            $statement = DB::select("SHOW TABLE STATUS LIKE 'partner_transactions'");
            $nextId = $statement[0]->Auto_increment;
            $hashids = new Hashids('', 6,'abcdefghijklmnopqrstuvwxyz123456789');
            $code = $hashids->encode($nextId);

            $partner_id = $request->partner_id;
            $type = $request->type;
            $giver = $request->giver;
            $receiver = $request->receiver;
            $note = $request->note;

            $product_ids = $request->product_id;
            $product_names = $request->product_name;
            
            $product_qtys = [];
            foreach ($product_ids as $key=>$_val) {
                $product_qtys[$key] = sizeof($request->product_serial_number[$key]);
            }
            
            $product_serial_numbers = $request->product_serial_number;
            $product_barcodes = $request->product_barcode;

            DB::beginTransaction();

            $pt = new PartnerTransactions;
            $pt->partner_id = $partner_id;
            $pt->type = $type;
            $pt->code = $code;
            $pt->giver = $giver;
            $pt->receiver = $receiver;
            $pt->note = $note;
            $pt->save();
            $pt_id = $pt->id;

            $products = [];

            foreach ($product_ids as $key=>$val) {
                $pp = new PartnerProducts;
                $pp->partner_id = $partner_id;
                $pp->partner_transaction_id = $pt_id;
                $pp->product_id = $product_ids[$key];
                $pp->product_name = $product_names[$key];
                $pp->qty = $product_qtys[$key];
                $pp->save();
                $pp_id = $pp->id;
                
                foreach ($product_serial_numbers[$key] as $keyItem=>$value) {
                    $ps = new PartnerItems;
                    $ps->partner_id = $partner_id;
                    $ps->partner_transaction_id = $pt_id;
                    $ps->partner_product_id = $pp_id;
                    $ps->serial_number = strtoupper($product_serial_numbers[$key][$keyItem]);
                    $ps->barcode = strtoupper($product_barcodes[$key][$keyItem]);
                    $ps->save();

                    Items::where('barcode','=',$product_barcodes[$key][$keyItem])->where('type','=','2')->delete();
                    
                }
            }

            DB::commit();
            return redirect('inventories/partner/'.$partner_id)->with('msg_success', 'Transactions Created Successfully');

        }
    }

    public function partner_destroy($partner_id,$transaction_id)
    {
        DB::beginTransaction();

        $transaction = PartnerTransactions::find($transaction_id);

        $partner_items = PartnerItems::where('partner_transaction_id','=',$transaction_id);

        foreach($partner_items->get() as $_partem) {
            Items::where('type','=',2)
            ->where('barcode','=',$_partem->barcode)
            ->where('serial_number','=',$_partem->serial_number)
            ->delete();
        }

        $partner_items->delete();

        PartnerProducts::where('partner_transaction_id','=',$transaction_id)->delete();
        $transaction->delete();

        DB::commit();
        
        return redirect('inventories/partner/'.$partner_id)->with('msg_success', 'Transactions Removed Successfully');

    }

    public function tracking()
    {
        return view('inventories.tracking');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::find($id);
        $items = Items::where('product_id','=',$id)
                        ->where('type','=','1')
                        ->get();
        $partner_items = Items::where('product_id','=',$id)
                        ->where('type','=','2')
                        ->get();
        
        if($product->is_misc == 1 && $items->count() > 0) {
            $product->barcode = $items[0]->barcode;
        }
        return view('inventories.view',compact('product','items','partner_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function tanggal_indo($tanggal)
    {
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }
}
