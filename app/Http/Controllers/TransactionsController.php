<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Items;
use App\Packages;
use App\PackageProducts;
use App\Customers;
use App\CustomerPics;
use App\Operators;
use App\Guards;
use App\Quotations;
use App\Transactions;
use App\TransactionCarts;
use App\TransactionGuards;
use App\TransactionOthers;
use App\TransactionPayments;
use App\TransactionOperators;
use App\Inout;
use App\InoutItems;
use DB;
use PDF;
use Mail;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = DB::table('transactions')
                        ->select(
                            'transactions.*',
                            'ts.name as status_name',
                            DB::raw('sum(tp.amount) as paid_amount'),
                        )
                        ->leftJoin('transaction_statuses as ts','ts.code','=','transactions.status')
                        ->leftJoin('transaction_payments as tp','tp.transaction_id','=','transactions.id')
                        ->groupBy('transactions.id')
                        ->orderBy('transactions.created_at', 'DESC')
                        ->paginate(20);
        return view('transactions.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guards = Guards::All();
        $customers = Customers::All();
        $list = DB::select("SELECT
                        1 AS type,
                        prod.id AS uid,
                        prod.name AS name,
                        prod.price_day AS price_day,
                        prod.price_7days AS price_7days,
                        '' AS package_product
                    FROM
                        products prod
                    WHERE
                        prod.is_active = 1
                    UNION
                    SELECT
                        2 AS type,
                        pkg.id AS uid,
                        pkg.name AS name,
                        pkg.price_day AS price_day,
                        pkg.price_7days AS price_7days,
                        CONCAT('[',
                            GROUP_CONCAT(
                                JSON_OBJECT(
                                    'id',p.id,
                                    'category',p.category,
                                    'brand',p.brand,
                                    'name',p.name,
                                    'qty',pp.qty
                                )
                            ),
                            ']') AS package_product
                    FROM
                        packages pkg
                            INNER JOIN
                        package_products pp ON pkg.id = pp.package_id
                            INNER JOIN
                        products p ON p.id = pp.product_id
                    WHERE
                        pkg.is_active = 1
                    GROUP BY pkg.id");

        return view('transactions.create', compact('guards','customers','list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $customer = (object) [
            'customer_id' => $request->customer_id,
            'customer_type' => $request->customer_type,
            'customer_name' => $request->customer_name,
            'customer_phone' => $request->customer_phone,
            'customer_email' => $request->customer_email,
            // 'customer_address' => $request->customer_address,
            'customer_dop' => $request->customer_dop,
            'customer_gaffer' => $request->customer_gaffer,
            'customer_production' => $request->customer_production,
            'customer_location' => $request->customer_location,
            'customer_equipment_call' => $request->customer_equipment_call,
        ];

        $discounts = [];
        $discount[0] = (object) [
            'type' => '7d',
            'amount' => $request->cart_discount

        ];
        $discount[1] = (object) [
            'type' => 'add',
            'percentage' => $request->cart_add_discount,
            'amount' => $request->cart_add_discount_amount

        ];
        array_push($discounts, $discount);

        $custType = ($request->customer_type == 1) ? 'PSN' : 'VDR';
        $now = date('Y-m-d');
        $countTrans = Transactions::select(DB::raw('COUNT(id) as last_count'))->where(DB::raw('DATE(created_at)'),'=',$now)->first();


        $number = $custType.'/'.str_pad($countTrans['last_count']+1,3,'0',STR_PAD_LEFT).'/'.date('d-m-Y');

        DB::beginTransaction();
        $transaction = new Transactions;
        $transaction->customer_id = $request->customer_id;
        $transaction->number = $number;
        $transaction->customers = json_encode($customer);
        $transaction->start_date = $request->start_date;
        $transaction->end_date = $request->end_date;
        $transaction->days = $request->days;
        // $transaction->cart = json_encode($products);
        $transaction->cart_subtotal = $request->cart_subtotal;
        $transaction->cart_discounts = json_encode($discounts);
        $transaction->cart_total = $request->cart_total;
        // $transaction->guards = json_encode($guards);
        $transaction->guards_total = $request->guards_total;
        $transaction->others_total = $request->others_total;
        $transaction->total = $request->cart_total+$request->guards_total+$request->others_total;
        $transaction->save();
        $transaction_id = $transaction->id;

        $product_types = $request->product_type;
        $product_uids = $request->product_uid;
        $product_names = $request->product_name;
        $product_qtys = $request->product_qty;
        $product_days = $request->product_day;
        $product_prices = $request->product_price;
        $product_totals = $request->product_total;

        $products = [];
        $cart_id = 1;
        foreach ($request->product_type as $key=>$val) {
            $product = [];
            if($product_types[$key] == 1) {
                $product['transaction_id'] = $transaction_id;
                $product['number'] = $number;
                $product['cart_id']  = $cart_id;
                $product['type'] = $product_types[$key];
                $product['package_id'] = null;
                $product['package_name'] = null;
                $product['product_id'] = $product_uids[$key];
                $product['product_name'] = strip_tags($product_names[$key]);
                $product['package_product_qty'] = null;
                $product['qty'] = $product_qtys[$key];
                $product['day'] = $product_days[$key];
                $product['price'] = $product_prices[$key];
                $product['total'] = $product_totals[$key];
                array_push($products, $product);

            }
            elseif ($product_types[$key] == 2) {

                $package = Packages::where('id','=',$product_uids[$key])->first();
                $packageProducts = PackageProducts::where('package_id','=',$product_uids[$key])
                ->leftJoin('products','products.id','=','product_id')
                ->get();


                foreach ($packageProducts as $pp) {
                    $product['transaction_id'] = $transaction_id;
                    $product['number'] = $number;
                    $product['cart_id']  = $cart_id;
                    $product['type'] = $product_types[$key];
                    $product['package_id'] = $pp->package_id;
                    $product['package_name'] = $package['name'];
                    $product['product_id'] = $pp->product_id;
                    $product['product_name'] = $pp->name;
                    $product['package_product_qty'] = $pp->qty;
                    $product['qty'] = $product_qtys[$key];
                    $product['day'] = $product_days[$key];
                    $product['price'] = $product_prices[$key];
                    $product['total'] = $product_totals[$key];
                    array_push($products, $product);
                }
            }
            $cart_id++;
        }

        TransactionCarts::insert($products);

        if($request->guard_name){
            $guards = [];
            $guards_ids = $request->guard_id;
            $guards_names = $request->guard_name;
            $guards_qtys = $request->guard_qty;
            $guards_days = $request->guard_day;
            $guards_prices = $request->guard_price;
            $guards_totals = $request->guard_total;

            $guards = [];
            foreach ($request->guard_name as $key=>$val) {

                $guard = [];
                $guard['transaction_id'] = $transaction_id;
                $guard['number'] = $number;
                $guard['guard_id'] = $guards_ids[$key];
                $guard['name'] = strip_tags($guards_names[$key]);
                $guard['qty'] = $guards_qtys[$key];
                $guard['day'] = $guards_days[$key];
                $guard['price'] = $guards_prices[$key];
                $guard['total'] = $guards_totals[$key];
                array_push($guards, $guard);
            }

            TransactionGuards::insert($guards);
        }

        if($request->other_name){
            $others = [];
            $others_names = $request->other_name;
            $others_qtys = $request->other_qty;
            $others_prices = $request->other_price;
            $others_totals = $request->other_total;

            $others = [];
            foreach ($request->other_name as $key=>$val) {

                $other = [];
                $other['transaction_id'] = $transaction_id;
                $other['number'] = $number;
                $other['name'] = strip_tags($others_names[$key]);
                $other['qty'] = $others_qtys[$key];
                $other['price'] = $others_prices[$key];
                $other['total'] = $others_totals[$key];
                array_push($others, $other);
            }

            TransactionOthers::insert($others);
        }

        DB::commit();

        return redirect('transactions')->with('msg_success', 'Transactions Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $operator_list = Operators::all();
        $transaction = DB::table('transactions')
                        ->select(
                            'transactions.*',
                            'ts.name as status_name',
                            DB::raw('sum(tp.amount) as paid_amount'),
                            DB::raw('(sum(tp.amount)-total) as outstanding_amount')
                        )
                        ->leftJoin('transaction_statuses as ts','ts.code','=','transactions.status')
                        ->leftJoin('transaction_payments as tp','tp.transaction_id','=','transactions.id')
                        ->where('transactions.id','=',$id)
                        ->get()
                        ->first();

        $carts = TransactionCarts::where('transaction_id','=',$id)->get();

        $products = [];
        $package_id = '';

        foreach ($carts as $key=>$_cart) {
            if($_cart->type == 1) {
                $product = new \stdClass();
                $product->cart_id = $_cart->cart_id;
                $product->product_name = $_cart->product_name;
                $product->qty = $_cart->qty;
                $product->day = $_cart->day;
                $product->price = $_cart->price;
                $product->total = $_cart->total;
                array_push($products, $product);
            }
            elseif ($_cart->type == 2 && $package_id != $_cart->package_id) {
                $package_id = $_cart->package_id;
                $product = new \stdClass();
                $product->cart_id = $_cart->cart_id;
                $product->product_name = $_cart->package_name;
                $package_products = [];
                $product->product_name = $_cart->package_name;
                $product->product_name .= '<ul>';
                foreach ($carts as $_val) {
                    if($_val->package_id == $package_id) {
                        $pp = new \stdClass();
                        $product->product_name .= '<li>';
                        $product->product_name .= $_val->product_name;
                        $product->product_name .= ' x'.$_val->package_product_qty;
                        $product->product_name .= '</li>';
                    }
                }
                $product->product_name .= '</ul>';
                $product->qty = $_cart->qty;
                $product->day = $_cart->day;
                $product->price = $_cart->price;
                $product->total = $_cart->total;
                array_push($products, $product);
            }

        }

        $carts = $products;

        $guards = TransactionGuards::where('transaction_id','=',$id)->get();
        $others = TransactionOthers::where('transaction_id','=',$id)->get();

        // $payments = TransactionPayments::where('transaction_id','=',$id)->get();

        $payments = TransactionPayments::
                        select(DB::raw("
                                SUM(amount) as amounts,
                                CONCAT('[',
                                    GROUP_CONCAT(
                                        JSON_OBJECT(
                                            'sequence',
                                            sequence,
                                            'date',
                                            date,
                                            'name',
                                            name,
                                            'phone',
                                            phone,
                                            'type',
                                            type,
                                            'amount',
                                            amount,
                                            'desc',
                                            `desc`
                                        )
                                    ),
                                ']') AS list"))
                        ->where('transaction_id','=',$id)
                        ->first();
        $payments->amounts = ($payments->amounts) ? $payments->amounts : 0;
        $payments->list = ($payments->list) ? $payments->list : '[]';

        $inout = Inout::select(
                        'inout.*',
                        DB::raw("CONCAT('[',
                                    GROUP_CONCAT(
                                        JSON_OBJECT(
                                            'product_name',
                                            product_name,
                                            'barcode',
                                            item_barcode,
                                            'serial_no',
                                            item_serial_number
                                        )
                                    ),
                                ']') as items"))
                    ->leftJoin( 'inout_items','inout_items.inout_id','=','inout.id')
                    ->where('inout.transaction_id','=',$id)
                    ->groupby('inout.id')
                    ->orderBy('inout.created_at','DESC')
                    ->orderBy('inout_items.product_id','ASC')
                    ->get();

        $operators = TransactionOperators::where('transaction_id','=',$id)->get();

        return view('transactions.view', compact('transaction','operator_list','payments','carts','guards','others','inout','operators'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guards = Guards::All();
        $list = DB::select("SELECT
                        1 AS type,
                        prod.id AS uid,
                        prod.name AS name,
                        prod.price_day AS price_day,
                        prod.price_7days AS price_7days,
                        '' AS package_product
                    FROM
                        products prod
                    WHERE
                        prod.is_active = 1
                    UNION
                    SELECT
                        2 AS type,
                        pkg.id AS uid,
                        pkg.name AS name,
                        pkg.price_day AS price_day,
                        pkg.price_7days AS price_7days,
                        CONCAT('[',
                            GROUP_CONCAT(
                                JSON_OBJECT(
                                    'id',p.id,
                                    'category',p.category,
                                    'brand',p.brand,
                                    'name',p.name,
                                    'qty',pp.qty
                                )
                            ),
                            ']') AS package_product
                    FROM
                        packages pkg
                            INNER JOIN
                        package_products pp ON pkg.id = pp.package_id
                            INNER JOIN
                        products p ON p.id = pp.product_id
                    WHERE
                        pkg.is_active = 1
                    GROUP BY pkg.id");

        $transaction = Transactions::find($id);

        $cart_discounts = json_decode($transaction->cart_discounts,TRUE);
        $transaction->discount_percentage = $cart_discounts[0][1]['percentage'];

        $customer = json_decode($transaction->customers,TRUE);
        $carts = TransactionCarts::select(
                            'transaction_carts.*',
                            'products.price_day as prod_price_day',
                            'products.price_7days as prod_price_7days',
                            'packages.price_day as pkg_price_day',
                            'packages.price_7days as pkg_price_7days'
                        )
                        ->where('transaction_id','=',$id)
                        ->leftJoin('products','products.id','=','transaction_carts.product_id')
                        ->leftJoin('packages','packages.id','=','transaction_carts.package_id')
                        ->get();

        $products = [];
        $package_id = '';

        foreach ($carts as $key=>$_cart) {
            if($_cart->type == 1) {
                $product = new \stdClass();
                $product->cart_id = $_cart->cart_id;
                $product->product_id = $_cart->product_id;
                $product->product_name = $_cart->product_name;
                $product->type = $_cart->type;
                $product->qty = $_cart->qty;
                $product->day = $_cart->day;
                $product->price = $_cart->price;
                $product->total = $_cart->total;

                $product->price_day = $_cart->prod_price_day;
                $product->price_7days = $_cart->prod_price_7days;

                array_push($products, $product);
            }
            elseif ($_cart->type == 2 && $package_id != $_cart->package_id) {
                $package_id = $_cart->package_id;
                $product = new \stdClass();
                $product->cart_id = $_cart->cart_id;
                $product->product_id = $package_id;
                $product->product_name = $_cart->package_name;
                $product->type = $_cart->type;
                $package_products = [];
                $product->product_name = $_cart->package_name;
                $product->product_name .= '<ul>';
                foreach ($carts as $_val) {
                    if($_val->package_id == $package_id) {
                        $pp = new \stdClass();
                        $product->product_name .= '<li>';
                        $product->product_name .= $_val->product_name;
                        $product->product_name .= ' x'.$_val->package_product_qty;
                        $product->product_name .= '</li>';
                    }
                }
                $product->product_name .= '</ul>';
                $product->qty = $_cart->qty;
                $product->day = $_cart->day;
                $product->price = $_cart->price;
                $product->total = $_cart->total;

                $product->price_day = $_cart->pkg_price_day;
                $product->price_7days = $_cart->pkg_price_7days;

                array_push($products, $product);
            }

        }

        $carts = $products;

        $transaction_guards = TransactionGuards::where('transaction_id','=',$id)->get();
        $transaction_others = TransactionOthers::where('transaction_id','=',$id)->get();

        return view('transactions.edit',compact('guards','list','transaction','customer','carts','transaction_guards','transaction_others'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->_type == 'add_payment') {

            $sequence = 0;
            $amounts = 0;
            $paids = TransactionPayments::select('transaction_id',DB::raw('SUM(amount) as amounts'),DB::raw('MAX(sequence) as sequence'))
                                        ->where('transaction_id','=',$id)
                                        ->first();
            if($paids->sequence) {
                $sequence = $paids->sequence+1;
                $amounts = $paids->amounts;
            }

            $path = $request->file('file')->store('payments');

            $tp = new TransactionPayments;
            $tp->transaction_id = $request->transaction_id;
            $tp->sequence = $sequence;
            $tp->date = $request->date;
            $tp->type = $request->type;
            $tp->name = $request->name;
            $tp->phone = $request->phone;
            $tp->amount = $request->amount;
            $tp->desc = $request->desc;
            $tp->photo = $request->path;
            $tp->save();

            $amounts = $amounts+$request->amounts;

            $transaction = Transactions::find($id);
            if($sequence == 0) {
                $transaction->status = 2;
                $transaction->save();
            }
            if($transaction->total <= $amounts) {
                $transaction->status = 2;
                $transaction->is_paid = 2;
                $transaction->save();
            }

            return redirect('transactions/'.$id)->with('msg_success', 'Payment Added Successfully');
        }
        else if($request->_type == 'edit_transaction') {


            $customer = (object) [
                'customer_id' => $request->customer_id,
                'customer_type' => $request->customer_type,
                'customer_name' => $request->customer_name,
                'customer_phone' => $request->customer_phone,
                'customer_email' => $request->customer_email,
                // 'customer_address' => $request->customer_address,
                'customer_dop' => $request->customer_dop,
                'customer_gaffer' => $request->customer_gaffer,
                'customer_production' => $request->customer_production,
                'customer_location' => $request->customer_location,
                'customer_equipment_call' => $request->customer_equipment_call,
            ];

            $discounts = [];
            $discount[0] = (object) [
                'type' => '7d',
                'amount' => $request->cart_discount

            ];
            $discount[1] = (object) [
                'type' => 'add',
                'percentage' => $request->cart_add_discount,
                'amount' => $request->cart_add_discount_amount

            ];
            array_push($discounts, $discount);

            $transaction = Transactions::find($request->transaction_id);
            $transaction->version = $transaction->version+0.1;
            $transaction->customer_id = $request->customer_id;
            $transaction->customers = json_encode($customer);
            $transaction->start_date = $request->start_date;
            $transaction->end_date = $request->end_date;
            $transaction->days = $request->days;
            // $transaction->cart = json_encode($products);
            $transaction->cart_subtotal = $request->cart_subtotal;
            $transaction->cart_discounts = json_encode($discounts);
            $transaction->cart_total = $request->cart_total;
            // $transaction->guards = json_encode($guards);
            $transaction->guards_total = $request->guards_total;
            $transaction->others_total = $request->others_total;
            $transaction->total = $request->cart_total+$request->guards_total+$request->others_total;
            $transaction->save();

            $transaction_id = $request->transaction_id;
            $number = $transaction->number;

            TransactionCarts::where('transaction_id','=',$request->transaction_id)->delete();
            TransactionGuards::where('transaction_id','=',$request->transaction_id)->delete();
            TransactionOthers::where('transaction_id','=',$request->transaction_id)->delete();

            $product_types = $request->product_type;
            $product_uids = $request->product_uid;
            $product_names = $request->product_name;
            $product_qtys = $request->product_qty;
            $product_days = $request->product_day;
            $product_prices = $request->product_price;
            $product_totals = $request->product_total;

            $product_types = $request->product_type;
            $product_uids = $request->product_uid;
            $product_names = $request->product_name;
            $product_qtys = $request->product_qty;
            $product_days = $request->product_day;
            $product_prices = $request->product_price;
            $product_totals = $request->product_total;

            $products = [];
            $cart_id = 1;
            foreach ($request->product_type as $key=>$val) {
                $product = [];
                if($product_types[$key] == 1) {
                    $product['transaction_id'] = $transaction_id;
                    $product['number'] = $number;
                    $product['cart_id']  = $cart_id;
                    $product['type'] = $product_types[$key];
                    $product['package_id'] = null;
                    $product['package_name'] = null;
                    $product['product_id'] = $product_uids[$key];
                    $product['product_name'] = strip_tags($product_names[$key]);
                    $product['package_product_qty'] = null;
                    $product['qty'] = $product_qtys[$key];
                    $product['day'] = $product_days[$key];
                    $product['price'] = $product_prices[$key];
                    $product['total'] = $product_totals[$key];
                    array_push($products, $product);

                }
                elseif ($product_types[$key] == 2) {

                    $package = Packages::where('id','=',$product_uids[$key])->first();
                    $packageProducts = PackageProducts::where('package_id','=',$product_uids[$key])
                    ->leftJoin('products','products.id','=','product_id')
                    ->get();


                    foreach ($packageProducts as $pp) {
                        $product['transaction_id'] = $transaction_id;
                        $product['number'] = $number;
                        $product['cart_id']  = $cart_id;
                        $product['type'] = $product_types[$key];
                        $product['package_id'] = $pp->package_id;
                        $product['package_name'] = $package['name'];
                        $product['product_id'] = $pp->product_id;
                        $product['product_name'] = $pp->name;
                        $product['package_product_qty'] = $pp->qty;
                        $product['qty'] = $product_qtys[$key];
                        $product['day'] = $product_days[$key];
                        $product['price'] = $product_prices[$key];
                        $product['total'] = $product_totals[$key];
                        array_push($products, $product);
                    }
                }
                $cart_id++;
            }

            TransactionCarts::insert($products);

            if($request->guard_name){
                $guards = [];
                $guards_ids = $request->guard_id;
                $guards_names = $request->guard_name;
                $guards_qtys = $request->guard_qty;
                $guards_days = $request->guard_day;
                $guards_prices = $request->guard_price;
                $guards_totals = $request->guard_total;

                $guards = [];
                foreach ($request->guard_name as $key=>$val) {

                    $guard = [];
                    $guard['transaction_id'] = $transaction_id;
                    $guard['number'] = $number;
                    $guard['guard_id'] = $guards_ids[$key];
                    $guard['name'] = strip_tags($guards_names[$key]);
                    $guard['qty'] = $guards_qtys[$key];
                    $guard['day'] = $guards_days[$key];
                    $guard['price'] = $guards_prices[$key];
                    $guard['total'] = $guards_totals[$key];
                    array_push($guards, $guard);
                }

                TransactionGuards::insert($guards);
            }

            if($request->other_name){
                $others = [];
                $others_names = $request->other_name;
                $others_qtys = $request->other_qty;
                $others_prices = $request->other_price;
                $others_totals = $request->other_total;

                $others = [];
                foreach ($request->other_name as $key=>$val) {

                    $other = [];
                    $other['transaction_id'] = $transaction_id;
                    $other['number'] = $number;
                    $other['name'] = strip_tags($others_names[$key]);
                    $other['qty'] = $others_qtys[$key];
                    $other['price'] = $others_prices[$key];
                    $other['total'] = $others_totals[$key];
                    array_push($others, $other);
                }

                TransactionOthers::insert($others);
            }

            DB::commit();
            return redirect('transactions/'.$id)->with('msg_success', 'Transactions Updated Successfully');
        }
        else if($request->_type == 'add_operator') {
            $i = array();
            foreach($request->operator_pick as $_pick) {
                $raw = json_decode($_pick,TRUE);
                $d = array (
                    'transaction_id' => $request->transaction_id,
                    'operator_id' => $raw['id'],
                    'name' => $raw['name'],
                    'phone' => $raw['phone'],
                    'email' => $raw['email'],
                    'fee' => $raw['fee'],
                );
                array_push($i,$d);
            }
            TransactionOperators::insert($i);
            return redirect('transactions/'.$id)->with('msg_success', 'Operator Added Successfully');
        }
        else if($request->_type == 'delete_operator') {
            $transaction_operators = TransactionOperators::find($request->operator_id);
            $transaction_operators->delete();
            return redirect('transactions/'.$id)->with('msg_success', 'Operator Deleted Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dokumen($id,$action)
    {

        $transaction = Transactions::find($id);
        $carts = TransactionCarts::where('transaction_id','=',$id)
                    ->orderBy('cart_id','ASC')
                    ->get();

        $products = [];
        $package_id = '';

        foreach ($carts as $key=>$_cart) {
            if($_cart->type == 1) {
                $product = new \stdClass();
                $product->cart_id = $_cart->cart_id;
                $product->product_name = $_cart->product_name;
                $product->qty = $_cart->qty;
                $product->day = $_cart->day;
                $product->price = $_cart->price;
                $product->total = $_cart->total;
                array_push($products, $product);
            }
            elseif ($_cart->type == 2 && $package_id != $_cart->package_id) {
                $package_id = $_cart->package_id;
                $product = new \stdClass();
                $product->cart_id = $_cart->cart_id;
                $product->product_name = $_cart->package_name;
                $package_products = [];
                $product->product_name = $_cart->package_name;
                $product->product_name .= '<ul>';
                foreach ($carts as $_val) {
                    if($_val->package_id == $package_id) {
                        $pp = new \stdClass();
                        $product->product_name .= '<li>';
                        $product->product_name .= $_val->product_name;
                        $product->product_name .= ' x'.$_val->package_product_qty;
                        $product->product_name .= '</li>';
                    }
                }
                $product->product_name .= '</ul>';
                $product->qty = $_cart->qty;
                $product->day = $_cart->day;
                $product->price = $_cart->price;
                $product->total = $_cart->total;
                array_push($products, $product);
            }

        }

        $carts = $products;

        $guards = TransactionGuards::where('transaction_id','=',$id)->get();
        $others = TransactionOthers::where('transaction_id','=',$id)->get();
        $payments = TransactionPayments::
                        select(DB::raw("
                                SUM(amount) as amounts,
                                CONCAT('[',
                                    GROUP_CONCAT(
                                        JSON_OBJECT(
                                            'sequence',
                                            sequence,
                                            'date',
                                            date,
                                            'name',
                                            name,
                                            'phone',
                                            phone,
                                            'type',
                                            type,
                                            'amount',
                                            amount,
                                            'desc',
                                            `desc`
                                        )
                                    ),
                                ']') AS list"))
                        ->where('transaction_id','=',$id)
                        ->first();
        $payments->amounts = ($payments->amounts) ? $payments->amounts : 0;
        $payments->list = ($payments->list) ? $payments->list : '[]';

        $balance = new \stdClass();
        $balance->amount = $transaction->total-$payments->amounts;
        $balance->string = self::penyebut($balance->amount).'rupiah';
        $balance->dp = self::penyebut($balance->amount/100*50).'rupiah';

        $tanggal = self::tanggal_indo(date('Y-m-d',strtotime($transaction->created_at)));
        $tanggal_start = self::tanggal_indo(date('Y-m-d',strtotime($transaction->start_date)));
        $tanggal_end = self::tanggal_indo(date('Y-m-d',strtotime($transaction->end_date)));

        $customer = json_decode($transaction['customers'],TRUE);
        $custName = str_replace(' ', '', $customer['customer_name']);
        $custName = substr($custName, 0, 5);

        if($transaction->status < 1) {

            $namePdf = 'QUO#'.$transaction->number.'.pdf';
            $pdf = PDF::loadView('transactions.quotation', compact('transaction','carts','guards','others','balance','payments','tanggal','tanggal_start','tanggal_end'));

            if($action == 'email') {
                $data['to_name'] = 'NO-REPLY';
                $data['to_email'] = $customer['customer_email'];
                $data['subject'] = 'DOT RENTAL - '.'QUO#'.$transaction->number;
                Mail::send('transactions.quotation',  compact('transaction','carts','guards','others','balance','payments','tanggal','tanggal_start','tanggal_end'), function($message) use ($data, $pdf) {
                    $message->to($data['to_email'], $data['to_name'])
                    ->subject($data['subject'])
                    ->attachData($pdf->output(),$data['subject'].'.pdf');
                });
                return redirect('transactions/'.$id)->with('msg_success', 'Email Qutotation Send Successfully');
            }
            else if($action == 'unduh') {
                return $pdf->download($namePdf);
            }

            // return view('transactions.quotation', compact('transaction','carts','guards','others','balance','payments','tanggal','tanggal_start','tanggal_end'));
        }
        else {
            $namePdf = 'INV#'.$transaction->number.'.pdf';

            $pdf = PDF::loadView('transactions.invoice', compact('transaction','carts','guards','others','balance','payments','tanggal'));

            if($action == 'email') {
                $data['to_name'] = 'NO-REPLY';
                $data['to_email'] = $customer['customer_email'];
                $data['subject'] = 'DOT RENTAL - '.'INV#'.$transaction->number;
                Mail::send('transactions.invoice',  compact('transaction','carts','guards','others','balance','payments','tanggal'), function($message) use ($data, $pdf) {
                    $message->to($data['to_email'], $data['to_name'])
                    ->subject($data['subject'])
                    ->attachData($pdf->output(),$data['subject'].'.pdf');
                });
                return redirect('transactions/'.$id)->with('msg_success', 'Email Qutotation Send Successfully');
            }
            else if($action == 'unduh') {
                return $pdf->download($namePdf);
            }

            // return view('transactions.invoice', compact('transaction','carts','guards','others','balance','payments','tanggal'));
        }


    }

    private function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = self::penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = self::penyebut($nilai/10)." puluh". self::penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . self::penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = self::penyebut($nilai/100) . " ratus" . self::penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . self::penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = self::penyebut($nilai/1000) . " ribu" . self::penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = self::penyebut($nilai/1000000) . " juta" . self::penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = self::penyebut($nilai/1000000000) . " milyar" . self::penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = self::penyebut($nilai/1000000000000) . " trilyun" . self::penyebut(fmod($nilai,1000000000000));
		}
		return $temp;
    }

    private function tanggal_indo($tanggal)
    {
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }
}
