<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerTransactions extends Model
{
    protected $table = "partner_transactions";
	protected $primaryKey = "id";
}
