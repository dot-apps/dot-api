<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionGuards extends Model
{
    protected $table = "transaction_guards";
	protected $primaryKey = "id";
}
