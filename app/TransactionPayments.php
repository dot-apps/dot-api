<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionPayments extends Model
{
    protected $table = "transaction_payments";
	protected $primaryKey = "id";
}
